google.charts.load('current', {packages: ['corechart']});
        google.charts.setOnLoadCallback(drawCpuChart);
        google.charts.setOnLoadCallback(drawMemoryChart);
        google.charts.setOnLoadCallback(drawDiscChart);

        function drawCpuChart(){
            var cpu = new google.visualization.DataTable();
            cpu.addColumn('timeofday', 'momento');
            cpu.addColumn('number', 'porcentagem');
            cpu.addRows([
                [[8, 30, 00], 50],
                [[8, 31, 00], 55],
                [[8, 32, 00], 60],
                [[8, 33, 00], 40],
                [[8, 34, 00], 45]
            ]);
            var options = { 'title' : 'CPU Performance',
                            hAxis: {
                                title : 'horário', 
                                titleTextStyle : {
                                    color: '#000'
                            }},
                            vAxis: {
                                minValue : 0,
                                maxValue : 100
                            },
                            backgroundColor : {
                                fill : '#EFEFEF'
                            },
                            chartArea : {
                                backgroundColor : '#EFEFEF'
                            },
                            colors : ['#00A3FF']};
            var chart = new google.visualization.AreaChart(document.getElementById('cpu_graphic'));
            chart.draw(cpu, options);
        }

        function drawMemoryChart(){
            var cpu = new google.visualization.DataTable();
            cpu.addColumn('timeofday', 'momento');
            cpu.addColumn('number', 'porcentagem');
            cpu.addRows([
                [[8, 30, 00], 50],
                [[8, 31, 00], 55],
                [[8, 32, 00], 60],
                [[8, 33, 00], 40],
                [[8, 34, 00], 45]
            ]);
            var options = { 'title' : 'Memory Performance',
                            hAxis: {
                                title : 'horário', 
                                titleTextStyle : {
                                    color: '#000'
                            }},
                            vAxis: {
                                minValue : 0,
                                maxValue : 100
                            },
                            backgroundColor : {
                                fill : '#EFEFEF'
                            },
                            chartArea : {
                                backgroundColor : '#EFEFEF'
                            },
                            colors : ['#4200FF']};
            var chart = new google.visualization.AreaChart(document.getElementById('memory_graphic'));
            chart.draw(cpu, options);
        }
        function drawDiscChart(){
            var cpu = new google.visualization.DataTable();
            cpu.addColumn('timeofday', 'momento');
            cpu.addColumn('number', 'porcentagem');
            cpu.addRows([
                [[8, 30, 00], 50],
                [[8, 31, 00], 55],
                [[8, 32, 00], 60],
                [[8, 33, 00], 40],
                [[8, 34, 00], 45]
            ]);
            var options = { 'title' : 'Disc Performance',
                            hAxis: {
                                title : 'horário', 
                                titleTextStyle : {
                                    color: '#000'
                            }},
                            vAxis: {
                                minValue : 0,
                                maxValue : 100
                            },
                            backgroundColor : {
                                fill : '#EFEFEF'
                            },
                            chartArea : {
                                backgroundColor : '#EFEFEF'
                            },
                            colors : ['#AD00FF']};
            var chart = new google.visualization.AreaChart(document.getElementById('disc_graphic'));
            chart.draw(cpu, options);
        }
        function toggleSidebar(){
        document.getElementById("sidebar").classList.toggle('active');
        }