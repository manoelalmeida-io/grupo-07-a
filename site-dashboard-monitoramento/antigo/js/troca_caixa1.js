//Caixa 1
function caixa1() {
    
    document.getElementById("troca_div").innerHTML = `
    <div class="device">
            <div class="nome texto">CPU</div>
            <div class="alerta" id="alerta" style="background-color: yellow;"></div>
            <div class="informacao texto">Utilização: <span style="color: indigo;" id="utilizacao_cpu"></span> | Velocidade: <span>2.8GHz</span></div>
            <div class="informacao texto">Tempo de atividade: 1:2:27:32</div>

            <div class="descricao texto">
                AMD Ryzen 5 3500U with Radeon Vega Mobile Gfx
                <br>
                Clock base: 2,10GHz
                <br>
                Núcleos: 4
                <br>
                Processadores lógicos: 8
            </div>
        </div>
        <hr style="opacity: 0.3;">
        <div class="device device2">
            <div class="nome texto">MEMÓRIA</div>
            <div class="alerta" id="alerta" style="background-color: green;"></div>
            <div class="informacao texto">Utilização: <span style="color: indigo;" id="utilizacao_memoria"></span></div>
            <div class="informacao texto">Em uso: 4,5GB | Disponível: 9,9GB</div>

            <div class="descricao texto">
                Instalada: 12GB
                <br>
                Reservada para Hardware: 2,1GB
                <br>
                Velocidade: 2400MHz | DDR4 C16
                <br>
                Em cache: 5GB
            </div>
        </div>
        <hr style="opacity: 0.3;">
        <div class="device device2">
            <div class="nome texto">DISCO</div>
            <div class="alerta" id="alerta" style="background-color: red;"></div>
            <div class="informacao texto">Utilização: <span style="color: indigo;" id="utilizacao_disco"></span></div>
            <div class="informacao texto">Tempo médio de resposta: 30,4ms</div>

            <div class="descricao texto">
                ADATA SX6000LNP
                <br>
                Velocidade de leitura: 1800MB/s
                <br>
                Velocidade de escrita: 600MB/s
                <br>
                Interface: PCI-Express | Protocolo: NVMe
            </div>
        </div>
    `
}