//Caixa 2
function caixa2() {
    
    document.getElementById("troca_div").innerHTML = `
    <div class="device">
            <div class="nome texto">CPU</div>
            <div class="alerta" id="alerta" style="background-color: yellow;"></div>
            <div class="informacao texto">Utilização: <span style="color: indigo;" id="utilizacao_cpu"></span> | Velocidade: <span>2.8GHz</span></div>
            <div class="informacao texto">Tempo de atividade: 1:2:27:32</div>

            <div class="descricao texto">
                GeForce GTX 1060
                <br>
                Clock base: 2,10GHz
                <br>
                Núcleos: 4
                <br>
                Processadores lógicos: 8
            </div>
        </div>
        <hr style="opacity: 0.3;">
        <div class="device device2">
            <div class="nome texto">MEMÓRIA</div>
            <div class="alerta" id="alerta" style="background-color: green;"></div>
            <div class="informacao texto">Utilização: <span style="color: indigo;" id="utilizacao_memoria"></span></div>
            <div class="informacao texto">Em uso: 6,0GB | Disponível: 6,9GB</div>

            <div class="descricao texto">
                Instalada: 13GB
                <br>
                Reservada para Hardware: 3,1GB
                <br>
                Velocidade: 3000MHz | DDR4 C16
                <br>
                Em cache: 6GB
            </div>
        </div>
        <hr style="opacity: 0.3;">
        <div class="device device2">
            <div class="nome texto">DISCO</div>
            <div class="alerta" id="alerta" style="background-color: red;"></div>
            <div class="informacao texto">Utilização: <span style="color: indigo;" id="utilizacao_disco"></span></div>
            <div class="informacao texto">Tempo médio de resposta: 30,4ms</div>

            <div class="descricao texto">
                ADATA SX10000LNPPX
                <br>
                Velocidade de leitura: 2000MB/s
                <br>
                Velocidade de escrita: 800MB/s
                <br>
                Interface: PCI-Express-xMax | Protocolo: NVMe
            </div>
        </div>
    `
}