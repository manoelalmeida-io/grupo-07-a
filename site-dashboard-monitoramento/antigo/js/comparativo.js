Chart.defaults.global.defaultFontColor = '#ffffff';
Chart.defaults.global.defaultFontFamily = 'Roboto, sans-serif'

var modelosCtx = document.getElementById('modelos').getContext('2d');
var modelosConfig = {
  type: 'line',
  data: {
    labels: ['Dezembro', 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio'],
    datasets: [{
      label: 'Modelo 1',
      data: [4, 9, 8, 12, 15, 14],
      backgroundColor: 'rgba(255, 99, 132, 0.5)',
      borderColor: 'rgba(255, 99, 132, 1)',
      borderWidth: 4
    },
    {
      label: 'Modelo 2',
      data: [9, 9, 8, 10, 9, 8],
      backgroundColor: 'rgba(64, 198, 205, 0.5)',
      borderColor: 'rgba(64, 198, 205, 1)',
      borderWidth: 4
    },
    {
      label: 'Modelo 3',
      data: [20, 23, 22, 23, 25, 26],
      backgroundColor: 'rgba(64, 205, 71, 0.5)',
      borderColor: 'rgba(64, 205, 71, 1)',
      borderWidth: 4
    }]
  },
  options: {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        },
        scaleLabel: {
          display: true,
          labelString: 'Porcentagem'
        }
      }]
    },
    title: {
      display: true,
      text: 'Uso de CPU por modelo',
    },
    tooltips: {
      intersect: false,
      backgroundColor: '#FFF',
      titleFontColor: '#000',
      bodyFontColor: '#000',
      displayColors: false,
      callbacks: {
        label: (tooltipItem, data) => {
          var label = data.datasets[tooltipItem.datasetIndex].label;
          return label + ': ' + tooltipItem.yLabel + '% de uso de CPU';
        }
      }
    },
  }
};

var tipoMaquinaCtx = document.getElementById('tipo-maquina').getContext('2d');
var tipoMaquinaConfig = {
  type: 'doughnut',
  data: {
    labels: ['Totens', 'Caixas'],
    datasets: [{
      label: 'Modelo 1',
      data: [4, 9],
      backgroundColor: ['rgba(64, 198, 205, 1)', 'rgba(64, 205, 71, 1)'],
      borderColor: ['rgba(64, 198, 205, 1)', 'rgba(64, 205, 71, 1)'],
      borderWidth: 4,
    }],
  },
  options: {
    responsive: true,
    maintainAspectRatio: false,
    tooltips: {
      backgroundColor: '#FFF',
      titleFontColor: '#000',
      bodyFontColor: '#000',
      displayColors: false,
    }
  },
};

var modeloChart = new Chart(modelosCtx, modelosConfig);
var tipoMaquinaChart = new Chart(tipoMaquinaCtx, tipoMaquinaConfig);