package com.highstonks.monitoramento.oshi;

import com.highstonks.monitoramento.journal.Log;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Random;
import oshi.SystemInfo;
import oshi.hardware.CentralProcessor;
import oshi.hardware.ComputerSystem;
import oshi.hardware.HWDiskStore;
import oshi.hardware.PhysicalMemory;
import oshi.software.os.OperatingSystem;

public class StaticInfo {
    
    private static SystemInfo systemInfo = new SystemInfo();
    private static CentralProcessor.ProcessorIdentifier processor = systemInfo.getHardware().getProcessor().getProcessorIdentifier();
    private static PhysicalMemory[] memories = systemInfo.getHardware().getMemory().getPhysicalMemory();
    private static HWDiskStore[] disks = systemInfo.getHardware().getDiskStores();
    private static OperatingSystem operatingSystem = systemInfo.getOperatingSystem();
    private static ComputerSystem computer = systemInfo.getHardware().getComputerSystem();
    private static Boolean tipoDeMaquina = null;
    private static Integer idMaquina = null;
    private static String loja = null;
    
    public static void readConfigFile(String path) {
        try {
            FileReader file = new FileReader(path);
            BufferedReader leitor = new BufferedReader(file);
            String[] info = leitor.readLine().split("\\|");
            idMaquina = Integer.parseInt(info[2]);
            tipoDeMaquina = info[3] == "Caixa";
            loja = info[4];
            leitor.close();
        } catch (Exception ex) { System.out.println(ex.getMessage()); }
    }
    
    public static void atualizarConfig() {
        new Log().escreverArquivo("config.txt", "INFO ", idMaquina.toString()+"|"+getTipoDeMaquinaToString()+"|"+loja, false);
    }
    
    public static String getProcessorInfo() {
        return  processor.getName() + " - " +
                processor.getIdentifier() + " - " +
                processor.getMicroarchitecture() + " - " +
                processor.getVendor();
    }
    
    public static String[] getMemoryInfo() {
        String[] memorias = new String[memories.length];
        for (int i = 0; i < memories.length; i++) {
            memorias[i] = 
                    memories[i].getManufacturer() + " - " +
                    memories[i].getMemoryType() + " - " +
                    String.format("%.2fGB", Math.pow(10, -9) * memories[i].getCapacity()) + " - " +
                    String.format("%.2fGHz", Math.pow(10, -9) * memories[i].getClockSpeed());
        }
        return memorias;
    }
    
    public static String getMemoryInfoToString() {
        String[] memoriasAsArray = getMemoryInfo();
        String memorias = "";
        for (String memoria : memoriasAsArray) {
            memorias += memoria + " | ";
        } return memorias;
    }
    
    public static String[] getDiskInfo() {
        String[] discos = new String[disks.length];
        for (int i = 0; i < disks.length; i++) {
            discos[i] =
                    disks[i].getModel() + " - " +
                    String.format("%.2fGB", Math.pow(10, -9) * disks[i].getSize());
        }
        return discos;
    }
    
    public static String getDiskInfoToString() {
        String[] discosAsArray = getDiskInfo();
        String discos = "";
        for (String disco : discosAsArray) {
            discos += disco + " | ";
        } return discos;
    }
    
    public static String getOperatingSystem() {
        return operatingSystem.toString();
    }
    
    public static String getComputer() {
        return  computer.getManufacturer() + " - " +
                computer.getBaseboard().getManufacturer() + " - " +
                computer.getFirmware().getManufacturer();
    }
    
    public static Integer getIdMaquina() {
        return idMaquina;
    }
    
    public static void setIdMaquina(Integer id) {
        idMaquina = id;
    }
    
    public static String getLoja() {
        return loja;
    }
    
    public static void setLoja(String emailLoja) {
        loja = emailLoja;
    }
    
    public static boolean getTipoDeMaquina() {
        return tipoDeMaquina;
    }
    
    public static String getTipoDeMaquinaToString() {
        return tipoDeMaquina == true ? "Caixa" : "Totem";
    }
    
    public static void setTipoDeMaquina(boolean tipo) {
        tipoDeMaquina = tipo;
    }
}
