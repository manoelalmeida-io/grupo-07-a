package com.highstonks.monitoramento.oshi;

import oshi.SystemInfo;
import oshi.hardware.CentralProcessor;
import oshi.hardware.CentralProcessor.TickType;

public class Cpu {

    private CentralProcessor cpu;
    private long oldTicks[];

    public Cpu() {
        this.cpu = new SystemInfo().getHardware().getProcessor();
        oldTicks = new long[TickType.values().length];
    }

    public Double getMediaFrequencia() {
        Long soma = 0L;
        for (Long freq : cpu.getCurrentFreq()) {
            soma += freq;
        }
        return soma / cpu.getLogicalProcessorCount() / Math.pow(10, 9);
    }

    public Double getPorcetagem() {
        Double d = cpu.getSystemCpuLoadBetweenTicks(oldTicks);
        oldTicks = cpu.getSystemCpuLoadTicks();
        return d * 100.0;
    }

    public Double getMaxFrequencia() {
        return cpu.getMaxFreq() / Math.pow(10, 9);
    }
}
