package com.highstonks.monitoramento.oshi;

import oshi.SystemInfo;
import oshi.hardware.GlobalMemory;

public class Memoria {

    GlobalMemory ram;

    public Memoria() {
        this.ram = new SystemInfo().getHardware().getMemory();
    }

    public Double getMemoriaEmUso() {
        return (ram.getTotal() - ram.getAvailable()) / Math.pow(10, 9);
    }

    public Double getMemoriaDisponivel() {
        return ram.getAvailable() / Math.pow(10, 9);
    }

    public Double getPorcentagem() {
        Long total = ram.getTotal();
        Long emUso = total - ram.getAvailable();
        return (emUso * 100.0) / total;
    }
}
