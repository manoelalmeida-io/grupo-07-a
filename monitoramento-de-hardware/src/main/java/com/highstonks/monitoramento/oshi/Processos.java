package com.highstonks.monitoramento.oshi;

import oshi.SystemInfo;
import oshi.software.os.OSProcess;
import oshi.software.os.OperatingSystem;

public class Processos {
    
    private OSProcess processo[];
    
    public Processos() {
    SystemInfo si = new SystemInfo();
    OperatingSystem hal = si.getOperatingSystem();  
    this.processo = hal.getProcesses();    
    }
    
    public OSProcess[] getProcessess(){
        return processo;
    }
}
