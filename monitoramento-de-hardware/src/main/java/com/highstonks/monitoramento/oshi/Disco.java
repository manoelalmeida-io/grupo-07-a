package com.highstonks.monitoramento.oshi;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import oshi.SystemInfo;
import oshi.hardware.HWDiskStore;
import oshi.hardware.HWPartition;

public class Disco {

    private HWDiskStore[] discos;

    public Disco() {
        this.discos = new SystemInfo().getHardware().getDiskStores();
    }

    public String getNome(Integer indice) {
        return discos[indice].getModel();
    }

    public Integer getQuantidadeDiscos() {
        return discos.length;
    }

    public Double getEspaco(Integer indice) {
        Double tamanho = 0.0;
        for (String caminho : getCaminhoDisco(indice)) {  
            File armazenamento = new File(caminho);
            tamanho += (armazenamento.getTotalSpace() / Math.pow(10, 9));
        }
        return tamanho;
    }

    public List<String> getCaminhoDisco(Integer indice) {
        List<String> caminhosDeMontagem = new ArrayList<>();
        HWDiskStore disco = this.discos[indice];

        for (HWPartition partitions : disco.getPartitions()) {
            if (!partitions.getMountPoint().equals("")) {
                caminhosDeMontagem.add(partitions.getMountPoint());
            }
        }
        return caminhosDeMontagem;
    }

    public Double getEspacoLivre(Integer indice) {
        Double tamanho = 0.0;
        for (String caminho : getCaminhoDisco(indice)) {
            File armazenamento = new File(caminho);
            tamanho += (armazenamento.getFreeSpace() / Math.pow(10, 9));
        }
        return tamanho;
    }

    public Double getPorcetagem(Integer indice) {
        Double espacoTotal = getEspaco(indice);
        Double espacoLivre = getEspacoLivre(indice);
        return 100 - (espacoLivre * 100 / espacoTotal);
    }
}
