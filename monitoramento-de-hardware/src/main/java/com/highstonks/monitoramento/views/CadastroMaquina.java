package com.highstonks.monitoramento.views;

import com.highstonks.monitoramento.journal.Log;
import com.highstonks.monitoramento.oshi.StaticInfo;
import java.awt.Color;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;

public class CadastroMaquina extends javax.swing.JPanel {

    private JdbcTemplate template;
    private Log log;

    public CadastroMaquina(JdbcTemplate template) {
        this.template = template;
        log = new Log();
        initComponents();
        lblcadastro.setVisible(false);
        lblatualizar.setVisible(false);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        txtEmail = new javax.swing.JTextField();
        lblcadastro = new javax.swing.JLabel();
        lblatualizar = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtIdMaquina = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        comboTipoDeMaquina = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();

        jLabel4.setText("jLabel4");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Cadastrar uma nova máquina ");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Vincular a uma máquina registrada");

        lblcadastro.setBackground(new java.awt.Color(255, 255, 255));
        lblcadastro.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblcadastro.setForeground(new java.awt.Color(255, 255, 255));
        lblcadastro.setText("-");

        lblatualizar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblatualizar.setForeground(new java.awt.Color(255, 255, 255));
        lblatualizar.setText("-");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setText("E-mail da loja:");

        jButton1.setBackground(new java.awt.Color(164, 55, 58));
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setText("Cadastrar");
        jButton1.setBorderPainted(false);
        jButton1.setFocusPainted(false);
        jButton1.setPreferredSize(new java.awt.Dimension(70, 22));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setBackground(new java.awt.Color(164, 55, 58));
        jButton2.setForeground(new java.awt.Color(255, 255, 255));
        jButton2.setText("Atualizar");
        jButton2.setBorderPainted(false);
        jButton2.setFocusPainted(false);
        jButton2.setPreferredSize(new java.awt.Dimension(70, 22));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(51, 51, 51));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("CADASTRO");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel6)
        );

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel5.setText("ID da máquina existente:");

        comboTipoDeMaquina.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        comboTipoDeMaquina.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "----------", "Totem", "Caixa" }));

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setText("Tipo de máquina:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblcadastro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtIdMaquina, javax.swing.GroupLayout.DEFAULT_SIZE, 113, Short.MAX_VALUE)
                        .addGap(20, 20, 20)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblatualizar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtEmail, javax.swing.GroupLayout.DEFAULT_SIZE, 175, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(comboTipoDeMaquina, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(20, 20, 20)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jLabel1)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtEmail))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(comboTipoDeMaquina, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(19, 19, 19))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addComponent(lblcadastro)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jLabel2)
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtIdMaquina, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addComponent(lblatualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(17, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtIDmaquinaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIDmaquinaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIDmaquinaActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (StaticInfo.getIdMaquina() == null) {
            try {
                if (comboTipoDeMaquina.getSelectedItem().toString().equals("----------"))
                    throw new Exception("Nenhum tipo de máquina foi selecionado");
                List<String> lojas = template.queryForList("select id_loja from tb_loja where email like '"+txtEmail.getText()+"';", String.class);
                if (lojas.isEmpty())
                    throw new Exception("E-mail da loja inválido");
                boolean tipo = comboTipoDeMaquina.getSelectedItem().toString() == "Totem" ? true : false;
                StaticInfo.setTipoDeMaquina(tipo);
                String fk_modelo = verificarModelo_E_InserirNovo_SeNecessario(StaticInfo.getProcessorInfo(), StaticInfo.getMemoryInfoToString(), StaticInfo.getDiskInfoToString());
                template.update("insert into tb_maquina(tipo, fk_loja, fk_modelo)values (?,?,?)",
                    StaticInfo.getTipoDeMaquina(),
                    lojas.toArray()[0].toString(),
                    fk_modelo
                );
                List<String> maquinas = template.queryForList("select id_maquina from tb_maquina where fk_loja like '"+lojas.toArray()[0].toString()+"' and fk_modelo like '"+fk_modelo+"';", String.class);
                StaticInfo.setIdMaquina(Integer.parseInt(maquinas.get(maquinas.size() - 1)));
                StaticInfo.setLoja(txtEmail.getText());
                StaticInfo.atualizarConfig();
                log.escreverArquivo("app.txt", "INFO ", "Máquina cadastrada com sucesso", true);
                lblcadastro.setForeground(Color.green);
                lblcadastro.setText("Máquina cadastrada com sucesso");
            }
            catch (Exception ex) {
                log.escreverArquivo("app.txt", "ERROR", ex.getMessage(), true);
                lblcadastro.setForeground(Color.red);
                lblcadastro.setText(ex.getMessage());
            }
        } else {
            lblcadastro.setForeground(Color.gray);
            lblcadastro.setText("Máquina já cadastrada");
        }
        lblcadastro.setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private List<String> getModelos(String processador, String memoria, String disco) {
        try {
            return template.queryForList("select id_modelo from tb_modelo where cpu like '"+processador+"' and memoria like '"+memoria+"' and disco like '"+disco+"';", String.class);
        }
        catch (Exception ex) {
            log.escreverArquivo("app.txt", "ERROR", ex.getMessage(), true);
            return null;
        }
    }
    
    private String verificarModelo_E_InserirNovo_SeNecessario(String processador, String memoria, String discoPrimario) {
        try {
            List<String> modelos = getModelos(processador, memoria, discoPrimario);
            String fk_modelo;
            if (modelos.isEmpty()) {
                template.update("insert into tb_modelo (nome, cpu, memoria, disco) values (?,?,?,?)",
                    StaticInfo.getComputer(),
                    processador,
                    memoria,
                    discoPrimario
                );
                modelos = getModelos(processador, memoria, discoPrimario);
            }
            fk_modelo = modelos.get(0);
            return fk_modelo;
        }
        catch (Exception ex){
            log.escreverArquivo("app.txt", "ERROR", ex.getMessage(), true);
            throw ex;
        }
    }
    
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if (!txtIdMaquina.getText().equals("")) {
            try {
                Integer id = Integer.parseInt(txtIdMaquina.getText());
                List maquinas = template.queryForList("select * from tb_maquina where id_maquina = ?", id);
                if (maquinas.size() <= 0)
                    throw new Exception("O ID informado é inválido");
                List<String> lojas = template.queryForList("select email from tb_loja where id_loja = (select fk_loja from tb_maquina where id_maquina = "+id+")", String.class);
                if (!lojas.get(0).equals(StaticInfo.getLoja()))
                    throw new Exception("O ID da máquina desejada para vincular a esta pertence a outra loja");
                template.update("update tb_maquina set fk_modelo = ? where id_maquina = ?",
                    verificarModelo_E_InserirNovo_SeNecessario(StaticInfo.getProcessorInfo(), StaticInfo.getMemoryInfoToString(), StaticInfo.getDiskInfoToString()),
                    id.toString()
                );
                StaticInfo.setIdMaquina(id);
                StaticInfo.atualizarConfig();
                lblatualizar.setForeground(Color.green);
                lblatualizar.setText("Máquina atualizada com sucesso");
                log.escreverArquivo("app.txt", "INFO ", "Máquina atualizada com sucesso", true);
            } catch (Exception ex) {
                log.escreverArquivo("app.txt", "WARN ", ex.getMessage(), true);
                lblatualizar.setForeground(Color.red);
                lblatualizar.setText(ex.getMessage());
            } finally {
                lblatualizar.setVisible(true);
            }
        } else {
            lblatualizar.setVisible(false);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> comboTipoDeMaquina;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblatualizar;
    private javax.swing.JLabel lblcadastro;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtIdMaquina;
    // End of variables declaration//GEN-END:variables
}
