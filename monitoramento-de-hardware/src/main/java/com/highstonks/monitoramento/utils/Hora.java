package com.highstonks.monitoramento.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Hora {

    private DateTimeFormatter formatador;

    public Hora() {
        this.formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
    }

    public String agora() {
        LocalDateTime dataHora = LocalDateTime.now();
        return dataHora.format(formatador);
    }
}
