package com.highstonks.monitoramento.journal;

import com.highstonks.monitoramento.oshi.StaticInfo;
import java.time.LocalDateTime;
import org.springframework.jdbc.core.JdbcTemplate;

public class Registro {

    private LocalDateTime momento;
    private Integer regCPU;
    private Integer regDisco;
    private Integer regMemoria;
    private JdbcTemplate template;
    private Log log;

    public Registro(JdbcTemplate template) {

        this.template = template;
        log = new Log();
    }

    public void inserirDados() {
        try {
            template.update("INSERT INTO tb_registro (momento, reg_cpu, reg_memoria, reg_disco, fk_maquina) VALUES (?,?,?,?,?)",
                getMomento(),
                getRegCPU(),
                getRegMemoria(),
                getRegDisco(),
                StaticInfo.getIdMaquina().toString()
            );
            log.escreverArquivo("app.txt", "INFO ", "Registro inserido no banco", true);
        }
        catch (Exception ex) {
            log.escreverArquivo("app.txt", "ERROR", ex.getMessage(), true);
        }
    }

    public LocalDateTime getMomento() {
        return momento;
    }

    public void setMomento(LocalDateTime momento) {
        this.momento = momento;
    }

    public Integer getRegCPU() {
        return regCPU;
    }

    public void setRegCPU(Integer regCPU) {
        this.regCPU = regCPU;
    }

    public Integer getRegDisco() {
        return regDisco;
    }

    public void setRegDisco(Integer regDisco) {
        this.regDisco = regDisco;
    }

    public Integer getRegMemoria() {
        return regMemoria;
    }

    public void setRegMemoria(Integer regMemoria) {
        this.regMemoria = regMemoria;
    }
}
