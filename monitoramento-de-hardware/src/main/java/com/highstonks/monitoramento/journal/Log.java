package com.highstonks.monitoramento.journal;

import com.highstonks.monitoramento.utils.Hora;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Log {

    private Hora horario;
    private Registro registro;
    private final String DIR;

    public Log() {
        horario = new Hora();
        DIR = System.getProperty("java.io.tmpdir")+"high-stonks\\logs-monitoramento\\";
    }
    
    private String criarArquivo(String path) {
        try {
            new File(DIR).mkdirs();
            path = DIR+path;
            File log = new File(path);
            if (log.createNewFile()) {
                System.out.println("Arquivo criado " + log.getName());
            } else {
                System.out.println("Arquivo já existe.");
            }
            return path;
        } catch (IOException e) {
            System.out.println("Ocorreu um erro no log.");
            System.out.println(e.getMessage());
            return null;
        }
    }
    
    public void escreverArquivo(String path, String severity, String info, boolean append) {
        try {
            String archive = criarArquivo(path);
            FileWriter escritor = new FileWriter(archive, append);
            BufferedWriter escritorBuffer = new BufferedWriter(escritor);
            escritorBuffer.write(horario.agora() + "|" + severity + "|" + info + "\n");
            escritorBuffer.close();
            System.out.println("Escrito com sucesso para o arquivo.");
        } catch (IOException e) {
            System.out.println("Ocorreu um erro no log.");
            System.out.println(e.getMessage());
        }
    }

    public void escreverRegistro(Registro registro) {
        try {
            String archive = criarArquivo("hardware.txt");
            FileWriter escritor = new FileWriter(archive, true);
            BufferedWriter escritorBuffer = new BufferedWriter(escritor);
            escritorBuffer.write(String.format("%s|INFO |CPU %d%%|RAM %d%%|DISCO %d%%", horario.agora(), registro.getRegCPU(), registro.getRegMemoria(), registro.getRegDisco()));
            escritorBuffer.newLine();
            escritorBuffer.close();
            System.out.println("Escrito com sucesso para o arquivo.");
        } catch (IOException e) {
            System.out.println("Ocorreu um erro no log.");
            System.out.println(e.getMessage());
        }
    }
}
