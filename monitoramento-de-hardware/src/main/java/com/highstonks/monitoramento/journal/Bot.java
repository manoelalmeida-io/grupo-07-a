package com.highstonks.monitoramento.journal;

import com.highstonks.monitoramento.oshi.StaticInfo;
import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import javax.ws.rs.core.UriBuilder;

public class Bot {

    private int counter = 0;
    private final Integer CPU = 91;
    private final Integer DISCO = 83;
    private final Integer MEMORIA = 84;
    private static final String CHAT_ID = "-1001254943501";
    private static final String TOKEN = "1193123517:AAF5njbekuY7sPZXCTWkK0ABL2PWo7-OdG8";

    public void verificarParametrosParaEnviarAlerta(Registro registro) throws IOException, InterruptedException {
        int contador = 0;
        String mensagem = "Alerta: A máquina " + StaticInfo.getIdMaquina() + " está com problemas.\n";

        if (registro.getRegCPU() > CPU) {
            mensagem += "CPU está com " + registro.getRegCPU() + "% de uso.\n";
            contador++;
        }
        if (registro.getRegDisco() > DISCO) {
            mensagem += "O disco está com " + registro.getRegDisco() + "% de espaço de armazenamento cheio.\n";
            contador++;
        }
        if (registro.getRegMemoria() > MEMORIA) {
            mensagem += "A memória RAM está com " + registro.getRegMemoria() + "% de uso.\n";
            contador++;
        }
        if (contador > 0) {
            counter ++;
            if (counter >= 60){
                counter = 0;
                enviarMensagem(mensagem); 
            }
        } else counter = 0;
    }

    public void enviarMensagem(String message) throws IOException, InterruptedException {

        HttpClient client = HttpClient.newBuilder()
                .connectTimeout(Duration.ofSeconds(5))
                .version(HttpClient.Version.HTTP_2)
                .build();

        UriBuilder builder = UriBuilder
                .fromUri("https://api.telegram.org")
                .path("/{token}/sendMessage")
                .queryParam("chat_id", CHAT_ID)
                .queryParam("text", message);

        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(builder.build("bot" + TOKEN))
                .timeout(Duration.ofSeconds(5))
                .build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

        System.out.println(response.statusCode());
        System.out.println(response.body());
    }
}
