import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import api from '../../services/api';

import './styles.css';

export default function Checkout() {
  const [number, setNumber] = useState(1);
  const history = useHistory();

  useEffect(init, []);

  function init() {
    const global = JSON.parse(localStorage.getItem('global'));
    
    if (!global) {
      localStorage.setItem('global', JSON.stringify({ number: 1 }));
    }
    else {
      localStorage.setItem('global', JSON.stringify({ number: global.number + 1 }));
      setNumber((global.number + 1) % 100);
    }

    /* persist(); */
  }

  async function persist() {
    const cart = JSON.parse(localStorage.getItem('cart'));
    const takeout = localStorage.getItem('takeout') === 'true';
    const card = localStorage.getItem('card') === 'true';

    console.log(cart);

    await api.post('/orders', {
      viagem: takeout,
      cartao: card,
      data: new Date(),
      produtos: cart.itens.map(item => ({
        produto: item.item.id,
        qtd: item.qtd
      })),
      combos: cart.combos.map(item => ({
        combo: item.item.id,
        qtd: item.qtd
      }))
    });
  }

  function restart() {
    history.push('/');
  }

  return (
    <div className="checkout-container" onClick={restart}>
      <div className="content">
        <p>Número do pedido: {number}</p>
        <h1>Seu pedido foi finalizado!</h1>
      </div>
    </div>
  );
}