import React from 'react';
import { useHistory } from 'react-router-dom';

import NumberCircle from '../../components/NumberCircle';
import OptionCard from '../../components/OptionCard';

import CardIcon from '../../assets/icons/card.png';
import MoneyIcon from '../../assets/icons/money.png';

export default function ChoosePayment() {
  const history = useHistory();

  function next(card) {
    localStorage.setItem('card', card);
    history.push('/verification');
  }

  return (
    <div className="choose-place-container">
      <div className="content">
        <header>
          <NumberCircle>3</NumberCircle>
          <span>Como deseja pagar?</span>
        </header>
        <main>
          <OptionCard onClick={() => next(true)} image={CardIcon}>Cartão</OptionCard>
          <OptionCard onClick={() => next(false)} image={MoneyIcon}>Dinheiro</OptionCard>
        </main>
      </div>
    </div>
  );
}