import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { MdShoppingCart } from 'react-icons/md';

import NumberCircle from '../../components/NumberCircle';
import ListItem from '../../components/ListItem';
import Button from '../../components/Button';
import ButtonIcon from '../../components/ButtonIcon';
import CartModal from '../../components/CartModal';

import './styles.css';

export default function ChooseFood() {
  const [cartModal, setCartModal] = useState({ show: false });
  const history = useHistory();

  function openCartModal(item) {
    setCartModal({ show: true, item });
  }

  function closeCartModal() {
    setCartModal({ show: false });
  }

  function goToPayment() {
    history.push('payment');
  }

  return (
    <div className="choose-food-container">
      <div className="content">
        <header>
          <div className="title">
            <NumberCircle>2</NumberCircle>
            <span>Faça a sua escolha</span>
          </div>
          <div className="buttons">
            <ButtonIcon Icon={MdShoppingCart} onClick={() => openCartModal({})}/>
            <Button onClick={goToPayment}>Continuar</Button>
          </div>
        </header>
        <main>
          <ListItem />
        </main>
      </div>
      <CartModal config={cartModal} onClose={closeCartModal} />
    </div>
  );
}