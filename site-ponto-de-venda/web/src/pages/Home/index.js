import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import MastercardIcon from '../../assets/icons/mastercard.png';
import VisaIcon from '../../assets/icons/visa.png';
import FastFoodRestaurantImg from '../../assets/images/fast-food-restaurant.png';

import './styles.css';

export default function Home() {
  const history = useHistory();

  useEffect(() => {
    localStorage.setItem('cart', JSON.stringify({ combos: [], itens: [] }))
  }, []);

  function next() {
    history.push('/choose/place');
  }

  return (
    <div className="home-container" onClick={next}>
      <div className="content">
        <main>
          <div>
            <h1>Bem-vindo</h1>
            <p>Faça o seu pedido usando nosso totem</p>
          </div>
          <div className="bottom">
            <p>Toque aqui para iniciar</p>
            <div className="card-brands">
              <img id="visa-icon" src={VisaIcon} alt=""/>
              <img src={MastercardIcon} alt=""/>
            </div>
          </div>
        </main>
        <div>
          <img src={FastFoodRestaurantImg} alt=""/>
        </div>
      </div>
    </div>
  );
}