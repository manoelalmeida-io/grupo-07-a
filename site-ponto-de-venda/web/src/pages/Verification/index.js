import React from 'react';

import NumberCircle from '../../components/NumberCircle';
import Button from '../../components/Button';
import { useHistory } from 'react-router-dom';

import './styles.css';

export default function Verification() {
  const history = useHistory();

  function cancel() {
    history.push('/');
  }

  function next() {
    history.push('checkout');
  }

  function items() {
    const cart = JSON.parse(localStorage.getItem('cart'));

    if (cart) 
      return JSON.parse(localStorage.getItem('cart'));
    else
      return { combos: [], itens: [] };
  }

  function total() {
    const sumItems = items().itens.reduce((prev, item) => prev + (item.item.preco * item.qtd), 0);
    const sumCombos = items().combos.reduce((prev, item) => prev + (item.item.preco * item.qtd), 0);

    return sumItems + sumCombos;
  }

  function Summary() {
    const takeout = localStorage.getItem('takeout') === 'true';
    const card = localStorage.getItem('card') === 'true';

    return (
      <div>
        <p>{takeout ? 'Levar para viagem' : 'Comer aqui'}</p>
        <p>{`${card ? 'Cartão de crédito': 'Pagar com dinheiro'} 1x - R$${total().toFixed(2)}`}</p>
      </div>
    )
  }

  return (
    <div className="verification-container">
      <div className="content">
        <div className="card">
          <NumberCircle className="number">4</NumberCircle>
          <h1>Pedido</h1>
          <div className="items">
            {items().itens.map(item => (
              <div key={`i${item.item.id_produto}`} className="item">
                <input name="item" type="hidden" value={`${item.item.id_produto}`}/>
                <p>{`${item.item.nome} - ${item.qtd}x`}</p>
                <p>{`R$${(item.item.preco * item.qtd).toFixed(2)}`}</p>
              </div>
            ))}
            {items().combos.map(item => (
              <div key={`c${item.item.id_combo}`} className="item">
                <input name="combo" type="hidden" value={item.item.id_combo}/>
                <p>{`${item.item.nome} - ${item.qtd}x`}</p>
                <p>{`R$${(item.item.preco * item.qtd).toFixed(2)}`}</p>
              </div>
            ))}
          <div><p className="total">{`R$${total().toFixed(2)}`}</p></div>
          </div>
        </div>
        <div className="overview">
          <div className="card">
            <h2>Resumo</h2>
            <div className="content">
              <Summary/>
              <div className="buttons">
                <Button onClick={cancel}>Cancelar</Button>
                <Button onClick={next}>Continuar</Button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}