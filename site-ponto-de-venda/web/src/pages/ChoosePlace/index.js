import React from 'react';
import { useHistory } from 'react-router-dom';

import NumberCircle from '../../components/NumberCircle';
import OptionCard from '../../components/OptionCard';

import EatHereIcon from '../../assets/icons/eathere.png';
import TakeOutIcon from '../../assets/icons/takeout.png';

import './styles.css';

export default function ChoosePlace() {
  const history = useHistory();

  function next(takeout) {
    localStorage.setItem('takeout', takeout);
    history.push('food');
  }

  return (
    <div className="choose-place-container">
      <div className="content">
        <header>
          <NumberCircle>1</NumberCircle>
          <span>O que você prefere?</span>
        </header>
        <main>
          <OptionCard onClick={() => next(true)} image={TakeOutIcon}>Levar pra viagem</OptionCard>
          <OptionCard onClick={() => next(false)} image={EatHereIcon}>Comer aqui</OptionCard>
        </main>
      </div>
    </div>
  );
}