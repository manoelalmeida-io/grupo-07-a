import React from 'react';

import './styles.css';

export default function OptionCard({ onClick, children, image }) {
  return (
    <div className="option-card" onClick={onClick}>
      <img className="image" src={image} alt=""/>
      <div className="text">{children}</div>
    </div>
  );
}