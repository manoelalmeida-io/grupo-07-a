import React from 'react';

import './styles.css';

export default function ClickableImage({ image, onClick }) {
    return (
        <div onClick={onClick} className="clickable-image">
            <img src={image} alt="" />
        </div>
    );
}