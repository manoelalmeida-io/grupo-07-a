import React from 'react';

import './styles.css';

export default function ItemCard({ icon, onClick }) {
  return (
    <div className="item-card" onClick={onClick}>
      <img className="image" src={icon} alt=""/>
    </div>
  );
}