import React, { useState, useEffect } from 'react';
import { MdClear } from 'react-icons/md';

import CartItem from '../../components/CartItem';

import './styles.css';

export default function CartModal({ config, onClose }) {
	const [cart, setCart] = useState({ combos: [], itens:[] });

	useEffect(() => {
		const cart = JSON.parse(localStorage.getItem('cart'));
		
		if(cart) {
			setCart(JSON.parse(localStorage.getItem('cart')));
		}
	}, [config]);

	function remove(cartItem) {
    const cart = JSON.parse(localStorage.getItem('cart'));

    if (cart.itens && cartItem.item.categoria !== 'combos') {
      const index = cart.itens.findIndex((element, index, array) => {
        return element.item.id === cartItem.item.id && element.qtd === cartItem.qtd;
      });

			cart.itens.splice(index, 1);
			setCart(cart);
      localStorage.setItem('cart', JSON.stringify(cart));
    }

    if (cart.combos && cartItem.item.categoria === 'combos') {
      const index = cart.combos.findIndex((element, index, array) => {
        return element.item.id === cartItem.item.id && element.qtd === cartItem.qtd;
      });

			cart.combos.splice(index, 1);
			setCart(cart);
      localStorage.setItem('cart', JSON.stringify(cart));
    }
  }

	function Items() {
		if (cart.itens.length > 0) {
			return (
				<>
					<h3>Itens:</h3>
					<div>
						{cart.itens.map((item, index) => (
							<CartItem key={index} cartItem={item} onRemove={remove} />
						))}
					</div>
				</>
			);
		}

		return <></>;
	}

	function Combos() {
		if (cart.combos.length > 0) {
			return (
				<>
					<h3>Combos:</h3>
					<div>
						{cart.combos.map((combo, index) => (
							<CartItem key={index} cartItem={combo} onRemove={remove} />
						))}
					</div>
				</>
			);
		}

		return <></>;
	}

	function Empty() {
		if (cart.itens.length <= 0 && cart.combos.length <= 0) {
			return (
				<div className="centered">Você não possui itens no carrinho :(</div>
			);
		}

		return <></>;
	}

  return (
		<div className={config.show ? 'modal-show' : 'modal-hide'}>
			<div className="card">
				<div className="content">
					<div className="title">
						<h1>Carrinho</h1>
						<button onClick={onClose}>
							<MdClear className="icon" size={24} color="#FFFFFF" />
						</button>
					</div>
					<div className="items">
						<Items/>
						<Combos/>
						<Empty/>
					</div>
				</div>
			</div>
		</div>
	);
}