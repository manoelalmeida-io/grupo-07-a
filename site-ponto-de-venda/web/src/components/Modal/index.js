import React, { useState } from 'react';
import { MdClear, MdRemove, MdAdd } from 'react-icons/md';

import ButtonIcon from '../ButtonIcon';

import './styles.css';

export default function Modal({ config, onClose }) {
	const [qtd, setQtd] = useState(0);

	function add() {
		setQtd(qtd + 1);
	}

	function remove() {
		setQtd(qtd - 1);
	}

	function close() {
		const cartItem = {
			item: config.item,
			qtd
		}
		onClose(cartItem);
		setQtd(0);
	}

	if (!config.item) {
		config.item = {
			nome: 'No content',
			descricao: 'No description',
			img: ''
		}
	}

	return (
		<div className={config.show ? 'modal-show' : 'modal-hide'}>
			<div className="card">
				<div className="content">
					<div className="title">
						<h2>{config.item.nome}</h2>
						<button onClick={close}>
							<MdClear className="icon" size={24} color="#FFFFFF" />
						</button>
					</div>
					<p>{config.item.descricao}</p>
					<div className="price">
						<p>Preço:</p>
						<h2>{`R$${config.item.preco}`}</h2>
					</div>
					<div className="qtd-control">
						<ButtonIcon Icon={MdRemove} onClick={remove} />
						<span>{qtd}</span>
						<ButtonIcon Icon={MdAdd} onClick={add} />
					</div>
					<div className="img-container">
						<img src={config.item.image_url} alt="" />
					</div>
				</div>
			</div>
		</div>
	);
}