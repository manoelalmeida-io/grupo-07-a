import React from 'react';

import './styles.css';

export default function NumberCircle({ children }) {
  return (
    <div className="number-circle">
      <div>{children}</div>
    </div>
  );
}