import React from 'react';

import './styles.css';

export default function ButtonIcon({ Icon, onClick, className }) {
  return (
    <button className={`button-icon ${className}`} onClick={onClick}>
      <Icon size={24} color="#FFFFFF"/>
    </button>
  );
}