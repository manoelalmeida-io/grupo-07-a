import React, { useState, useEffect } from 'react';
import { MdKeyboardArrowUp, MdKeyboardArrowDown } from 'react-icons/md';

import api from '../../services/api';

import Modal from '../Modal';
import ItemCard from '../ItemCard';
import ClickableImage from '../ClickableImage';

import OptBurguer from '../../assets/images/opts/opt_burguer.png';
import OptSnacks from '../../assets/images/opts/opt_snacks.png';
import OptDrinks from '../../assets/images/opts/opt_drinks.png';
import OptCombo from '../../assets/images/opts/opt_combo.png';

import './styles.css';
import ButtonIcon from '../ButtonIcon';

export default function ListItem() {
  const [data, setData] = useState([]);
  const [modal, setModal] = useState({ show: false });
  const [page, setPage] = useState(1);
  const [category, setCategory] = useState('lanches');
  const [qtdItems, setQtdItems] = useState(0);
  const [upDisabled, setUpDisabled] = useState(false);
  const [downDisabled, setDownDisabled] = useState(false);

  useEffect(() => {
    async function load() {
      let response;
      
      if (category !== 'combos')
        response = await api.get(`/produtos?categoria=${category}&page=${page}`);
      else
        response = await api.get(`/combos`);
  
      setData(response.data);
      console.log(response);
      setQtdItems(parseInt(response.headers['x-total-count']));
    }

    if (!localStorage.getItem('cart')) {
      localStorage.setItem('cart', JSON.stringify({ itens: [], combos: [] }));
    }

    console.log(page, qtdItems);

    if (page > 1) {
      setUpDisabled(false);
    }
    else {
      setUpDisabled(true);
    }

    if ((page * 12) < qtdItems) {
      setDownDisabled(false);
    }
    else {
      setDownDisabled(true);
    }

    load();
  }, [page, qtdItems, category]);

  async function changeCategory(category) {
    setCategory(category);
    setPage(1);
  }

  function openModal(item) {
    setModal({ show: true, item });
  }

  function closeModal(cartItem) {
    const mCart = JSON.parse(localStorage.getItem('cart'));

    if(cartItem.qtd > 0) {
      if (cartItem.item.categoria !== 'combos') {
        mCart.itens.push(cartItem);
      }
      else {
        mCart.combos.push(cartItem);
      }
      localStorage.setItem('cart', JSON.stringify(mCart));
    }
    setModal({ show: false });
  }

  function pageUp() {
    if (page > 1) {
      setPage(page - 1);
    }
  }

  function pageDown() {
    if ((page * 12) < qtdItems) {
      setPage(page + 1);
    }
  }

  return (
    <div className="list-item">
      <div className="content">
        <div className="controls">
          <div>
            <ClickableImage image={OptBurguer} onClick={() => changeCategory('lanches')} />
            <ClickableImage image={OptSnacks} onClick={() => changeCategory('acompanhamentos')} />
            <ClickableImage image={OptDrinks} onClick={() => changeCategory('bebidas')} />
            <ClickableImage image={OptCombo} onClick={() => changeCategory('combos')} />
          </div>
          <div>
            <ButtonIcon Icon={MdKeyboardArrowUp} onClick={pageUp} className={ upDisabled ? 'disabled' : '' } />
            <ButtonIcon Icon={MdKeyboardArrowDown} onClick={pageDown} className={ downDisabled ? 'disabled' : '' } />
          </div>
        </div>
        <div className="items">
          { data.map(item => (
            <ItemCard key={`${item.categoria}${item.id_produto ? item.id_produto : item.id_combo}`} icon={item.image_url} onClick={() => openModal(item)} />
          ))}
        </div>
      </div>
      <Modal config={modal} onClose={closeModal} />
    </div>
  );
}