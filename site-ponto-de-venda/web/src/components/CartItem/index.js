import React from 'react';
import { MdDelete } from 'react-icons/md';

import ButtonIcon from '../../components/ButtonIcon';

import './styles.css';

export default function CartItem({ cartItem, onRemove }) {

  function remove() {
    onRemove(cartItem);
  }

  return (
    <div className="cart-item">
      <div>{cartItem.item.nome} - x{cartItem.qtd}</div>
      <div className="qtd-control">
        <ButtonIcon Icon={MdDelete} onClick={remove} />
      </div>
    </div>
  );
}