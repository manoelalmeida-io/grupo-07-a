import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Home from './pages/Home';
import ChoosePlace from './pages/ChoosePlace';
import ChooseFood from './pages/ChooseFood';
import ChoosePayment from './pages/ChoosePayment';
import Verification from './pages/Verification';
import Checkout from './pages/Checkout';

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Home}/>
        <Route path="/choose/place" component={ChoosePlace}/>
        <Route path="/choose/food" component={ChooseFood}/>
        <Route path="/choose/payment" component={ChoosePayment}/>
        <Route path="/verification" component={Verification}/>
        <Route path="/checkout" component={Checkout}/>
      </Switch>
    </BrowserRouter>
  );
}