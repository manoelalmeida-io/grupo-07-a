## Executando o simulador de ponto de venda

No terminal você deve executar os seguintes comandos.

### `npm install`

Este comando irá instalar todas as dependências do projeto.<br/>
Quando a execução terminar podemos ir para o próximo passo.

### `npm install -g json-server`
Caso ainda não tenha instalado o json-server voce pode usar o comando acima para instalar o json-server globalmente. 

### `json-server --watch -p 3333 db.json`

Esse comando irá iniciar o servidor json-server na porta 3333.<br/>
O json-server é uma pequena biblioteca que transforma um arquivo json em um servidor REST.<br/>
É desse servidor que estão sendo tirados os dados de produtos e onde os pedidos serão guardados.

### Endpoints
Abra [http://localhost:3333/itens](http://localhost:3333/itens) para visualizar todos os produtos. <br/>
Abra [http://localhost:3333/combos](http://localhost:3333/combos) para visualizar todos os combos. <br/>
Abra [http://localhost:3333/orders](http://localhost:3333/orders) para visualizar os pedidos já feitos.

### `npm start`

Abre o aplicativo de totem em modo de desenvolvimento.<br />
Abra [http://localhost:3000](http://localhost:3000) para visualizar no navegador.
