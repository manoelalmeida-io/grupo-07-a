const { Router } = require('express');
const ExportController = require('./controllers/ExportController');
const ItemsController = require('./controllers/ItemsController');
const CombosController = require('./controllers/CombosController');

const routes = Router();
const exportController = new ExportController();
const itemsController = new ItemsController();
const combosController = new CombosController();

routes.get('/export/items', exportController.products);
routes.get('/export/combos', exportController.combos);
routes.get('/produtos', itemsController.index);
routes.get('/combos', combosController.index);

module.exports = routes;