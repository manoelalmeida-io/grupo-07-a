const knex = require('../database/connection');

class CombosController {
  async index(request, response) {
    const { page = 1, limit = 12 } = request.query;

    const combos = await knex('tb_combo').select('*')
      .limit(limit)
      .offset((page - 1) * limit)
      .orderBy('id_combo', 'asc');
    
    const parsedCombos = combos.map(async (combo) => {
      const products = await knex('tb_produto_combo')
        .where('fk_combo', String(combo.id_combo)).select('*');

      return {
        ...combo,
        products
      };
    });

    const res = await Promise.all(parsedCombos);
    
    return response.json(res);
  }
}

module.exports = CombosController;