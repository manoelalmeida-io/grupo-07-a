const knex = require('../database/connection');
const axios = require('axios');

class ExportController {
  async products(request, response) {
    const persist = false;
    const items = await axios.get('http://localhost:3000/itens');
  
    const parsedItems = items.data.map(item => ({
      nome: item.nome,
      descricao: item.descricao,
      preco: item.preco,
      categoria: item.categoria,
      image_url: `http://localhost:3333/assets/images${item.img}`,
      fk_empresa: 1
    }));
  
    if (persist) {
      const promises = parsedItems.map(item => knex('tb_produto').insert(item));
      await Promise.all(promises);
    }
  
    return response.json(parsedItems);
  }

  async combos(request, response) {
    const persist = false;
    const combos = await axios.get('http://localhost:3000/combos');
    const parsedCombos = [];

    combos.data.forEach(async combo => {
      const parsedCombo = {
        nome: combo.nome,
        descricao: combo.descricao,
        preco: combo.preco,
        categoria: combo.categoria,
        image_url: `http://localhost:3333/assets/images${combo.img}`,
        fk_empresa: 1,
      }

      const parsedProducts = combo.lanches.map(produto => ({
        fk_produto: produto.id,
        quantidade: produto.qtd,
      }));

      if (persist) {
        const trx = await knex.transaction();

        const insertedIds = await knex('tb_combo').returning('id_combo').insert(parsedCombo);
        const comboId = insertedIds[0];

        const promises = parsedProducts.map(product => knex('tb_produto_combo').insert({
          fk_combo: comboId,
          ...product
        }));

        await Promise.all(promises);
        await trx.commit();
      }

      parsedCombos.push({
        ...parsedCombo,
        products: parsedProducts
      });
    });

    return response.json(parsedCombos);
  }
}

module.exports = ExportController;