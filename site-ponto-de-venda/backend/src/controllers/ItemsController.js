const knex = require('../database/connection');

class ItemsController {
  async index(request, response) {
    const { page = 1, limit = 12, categoria } = request.query;

    const [count] = await knex('tb_produto')
      .modify(builder => {
        if (categoria) {
          builder.where('categoria', categoria)
        }
      })
      .count();
    
    const items = await knex('tb_produto')
      .modify(builder => {
        if (categoria) {
          builder.where('categoria', categoria)
        }
      })
      .limit(limit)
      .offset((page - 1) * limit)
      .select('*')
      .orderBy('id_produto', 'asc');

    response.header('Access-Control-Expose-Headers', 'X-Total-Count');
    response.header('X-Total-Count', count['']);
    
    return response.json(items);
  }
}

module.exports = ItemsController;