﻿using OpenQA.Selenium;
using SimuladorCliente.Cmd;
using SimuladorCliente.Data.Context;
using System;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace SimuladorCliente.App
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "SIMULADOR DE CLIENTE - HIGH STONKS";
            Console.ForegroundColor = ConsoleColor.DarkRed;

            Console.WriteLine(
                "\n\t********************************************" +
                "\n\t*                                          *" +
                "\n\t*               HIGH STONKS                *" +
                "\n\t*           Simulador de cliente           *" +
                "\n\t*            2º ADS A - BANDTEC            *" +
                "\n\t*                                          *" +
                "\n\t********************************************\n");

            Console.ForegroundColor = ConsoleColor.White;

            string command = string.Empty;
            while (command != "0")
            {
                Console.WriteLine(
                    "\n1 - Iniciar Simulação" +
                    "\n0 - Sair\n");
                command = Console.ReadLine();
                try { if (command == "1") Execute(); }
                catch (Exception ex) { Console.WriteLine(ex.Message); }
            }
        }

        private static void Execute()
        {
            string delay = string.Empty;
            int result1;
            while (!int.TryParse(delay, out result1))
            {
                Console.WriteLine("\nTempo de delay em milisegundos:");
                delay = Console.ReadLine();
            }
            string times = string.Empty;
            int result2;
            while (!int.TryParse(times, out result2))
            {
                Console.WriteLine("\nRepetições da simulação:");
                times = Console.ReadLine();
            }
            string browser = string.Empty;
            bool result3 = true;
            while (true)
            {
                Console.WriteLine("\nEscolha o browser - Digite 'C' para Chrome ou 'F' para Firefox:");
                browser = Console.ReadLine();
                result3 = browser.ToUpper() == "C" ? true : false;
                if (browser.Equals("C") || browser.Equals("c") || browser.Equals("F") || browser.Equals("f"))
                    break;
            }

            Simulation sim = new Simulation(result3, 10, 10, result1);
            try { sim.Run("http://localhost:3000", result2); }
            catch { throw; }
        }
    }
}
