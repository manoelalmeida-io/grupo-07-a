﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SimuladorCliente.Data.Models
{
    [Table("tb_pedido_combo")]
    public class PedidoCombo
    {
        [Column("fk_pedido", Order = 0), Key]
        public int IdPedido { get; set; }

        [Column("fk_combo", Order = 1), Key]
        public int IdCombo { get; set; }

        [Column("quantidade", Order = 2)]
        public int Quantidade { get; set; }

        public override string ToString()
        {
            return $"ID Pedido: {IdPedido}\nID Combo: {IdCombo}\nQuantidade: {Quantidade}";
        }
    }
}
