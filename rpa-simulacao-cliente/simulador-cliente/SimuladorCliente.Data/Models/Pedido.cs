﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SimuladorCliente.Data.Models
{
    [Table("tb_pedido")]
    public class Pedido
    {
        [Column("id_pedido", Order = 0), Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdPedido { get; set; }

        [Column("datahora", Order = 1)]
        public DateTime DataHora { get; set; }

        [Column("fk_loja", Order = 2)]
        public int IdLoja { get; set; }

        [Column("fk_maquina", Order = 3)]
        public int IdMaquina { get; set; }

        public override string ToString()
        {
            return $"ID Pedido: {IdPedido}\nData/Hora: {DataHora}\nID Loja: {IdLoja}\nID Máquina: {IdMaquina}";
        }
    }
}
