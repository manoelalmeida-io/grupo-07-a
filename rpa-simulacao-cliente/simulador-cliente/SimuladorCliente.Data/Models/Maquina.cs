﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SimuladorCliente.Data.Models
{
    [Table("tb_maquina")]
    public class Maquina
    {
        [Column("id_maquina", Order = 0), Key]
        public int IdMaquina { get; set; }

        [Column("tipo", Order = 1)]
        public bool Tipo { get; set; }

        [Column("fk_loja", Order = 2)]
        public int IdLoja { get; set; }

        [Column("fk_modelo", Order = 3)]
        public int IdModelo { get; set; }

        private string GetTipo()
        {
            return Tipo == true ? "Totem" : "Caixa";
        }

        public override string ToString()
        {
            return $"ID Máquina: {IdMaquina}\nTipo: {GetTipo()}\nID Loja: {IdLoja}\nID Modelo: {IdModelo}";
        }
    }
}
