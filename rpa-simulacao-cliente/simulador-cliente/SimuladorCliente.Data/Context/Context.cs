﻿using SimuladorCliente.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;

namespace SimuladorCliente.Data.Context
{
    public class Context : DbContext
    {
        public Context() : base ("name=default")
        {
            Database.SetInitializer<Context>(null);
        }

        public virtual DbSet<Pedido> Pedido { get; set; }
        public virtual DbSet<PedidoProduto> PedidoProduto { get; set; }
        public virtual DbSet<PedidoCombo> PedidoCombo { get; set; }
        public virtual DbSet<Maquina> Maquina { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
