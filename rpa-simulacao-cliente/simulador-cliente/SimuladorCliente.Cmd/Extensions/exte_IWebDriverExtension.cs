﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;
using Helper = SeleniumExtras.WaitHelpers;
using System;
using System.Threading;

namespace SimuladorCliente.Cmd
{
    public static class IWebDriverExtension
    {
        /// <summary>
        /// Configures the implict timeouts and the explicit timeout of IWebDriver and WebDriverWait, respectively
        /// </summary>
        /// <param name="wait">The WebDriverWait instance that's used to wait a determined explicit timeout</param>
        /// <param name="implicitDelay">The implicit timeout in seconds</param>
        /// <param name="explicitDelay">The explicit timeout in seconds</param>
        public static void TimeoutConfig(this IWebDriver driver, ref WebDriverWait wait, int implicitDelay, int explicitDelay)
        {
            TimeSpan _implicitDelay = TimeSpan.FromSeconds(implicitDelay);
            driver.Manage().Timeouts().PageLoad.Add(_implicitDelay);
            driver.Manage().Timeouts().ImplicitWait.Add(_implicitDelay);
            driver.Manage().Timeouts().AsynchronousJavaScript.Add(_implicitDelay);

            TimeSpan _explicitDelay = TimeSpan.FromSeconds(explicitDelay);
            IClock clock = new SystemClock();
            wait = new WebDriverWait(clock, driver, _explicitDelay, _explicitDelay);
        }

        /// <summary>
        /// Waits for an element to be clickable to click
        /// </summary>
        /// <param name="wait">The WebDriverWait instance, which determines the timeout to wait for the element to be clickable</param>
        /// <param name="delay">The delay to click. Controls the simulation speed.</param>
        /// <param name="xPath">The XPath corresponding to the element you want to click</param>
        public static void ClickAt(this IWebDriver driver, WebDriverWait wait, int delay,string xPath)
        {
            IWebElement element = wait.Until(Helper.ExpectedConditions.ElementToBeClickable(By.XPath(xPath)));
            driver.ExecuteJavaScript("arguments[0].click()", element);
            Thread.Sleep(delay);
        }
    }
}
