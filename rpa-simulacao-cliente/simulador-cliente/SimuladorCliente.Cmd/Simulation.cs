﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using System.Text.RegularExpressions;
using System.Data.Entity.Infrastructure;

namespace SimuladorCliente.Cmd
{
    public class Simulation
    {
        private static IWebDriver driver;
        private static WebDriverWait wait;
        private List<string> items;
        private Register register;
        private int simulationDelay;
        
        public Simulation(bool isChrome, int implicitDelay, int explicitDelay, int simulationDelayInMilliseconds)
        {
            items = new List<string>();
            register = new Register();
            this.simulationDelay = simulationDelayInMilliseconds;
            if (isChrome)
                driver = new ChromeDriver();
            else
                driver = new FirefoxDriver();
            driver.TimeoutConfig(ref wait, implicitDelay, explicitDelay);
        }
        
        #region Simulation

        private void Start()
        {
            try
            {
                Random random = new Random();

                //Começar e escolher local para comer
                driver.ClickAt(wait, simulationDelay, "//div[@class='bottom']/p");
                driver.ClickAt(wait, simulationDelay, "//div[@class='option-card'][" + random.Next(1, 2) + "]");

                //Escolher lanche
                driver.ClickAt(wait, simulationDelay, "//div[@class='item-card'][" + random.Next(1, 12) + "]");
                Confirm();

                //Escolher acompanhamento
                driver.ClickAt(wait, simulationDelay, "//div[@class='clickable-image'][2]");
                driver.ClickAt(wait, simulationDelay, "//div[@class='item-card'][" + random.Next(1, 8) + "]");
                Confirm();

                //Escolher bebida
                driver.ClickAt(wait, simulationDelay, "//div[@class='clickable-image'][3]");
                driver.ClickAt(wait, simulationDelay, "//div[@class='item-card'][" + random.Next(1, 4) + "]");
                Confirm();

                //Escolher combo
                driver.ClickAt(wait, simulationDelay, "//div[@class='clickable-image'][4]");
                driver.ClickAt(wait, simulationDelay, "//div[@class='item-card'][" + random.Next(1, 4) + "]");
                Confirm();

                //Ver carrinho
                driver.ClickAt(wait, simulationDelay, "//div[@class='buttons']/button[@class='button-icon undefined']");
                driver.ClickAt(wait, simulationDelay, "//div[@class='modal-show']//div[@class='title']//button");
                driver.ClickAt(wait, simulationDelay, "//button[@class='button']");

                //Escolher forma de pagamento
                driver.ClickAt(wait, simulationDelay, "//div[@class='option-card'][" + random.Next(1, 2) + "]");

                //Pegar dados da tela
                this.items.Clear();
                var items = driver.FindElements(By.XPath("//div[@class='item']")).ToList();
                var itemsId = driver.FindElements(By.XPath("//input[@name='item']")).ToList();
                itemsId.AddRange(driver.FindElements(By.XPath("//input[@name='combo']")).ToList());
                for (int i = 0; i < items.Count; i++)
                    this.items.Add(items[i].Text + "#" + itemsId[i].GetAttribute("value").ToString());

                //Finalizar pedido
                driver.ClickAt(wait, simulationDelay, "//button[@class='button'][2]");
                driver.ClickAt(wait, simulationDelay, "//h1");
            }
            catch (Exception ex) { throw ex; }
        }
        
        private void Confirm()
        {
            try
            {
                int random = new Random().Next(1, 4);
                for (int i = 1; i <= random; i++)
                    driver.ClickAt(wait, simulationDelay, "//button[@class='button-icon undefined'][2]");
                driver.ClickAt(wait, simulationDelay, "//div[@class='modal-show']//div[@class='title']//button");
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion

        #region Caller

        public async Task Run(string url, int times)
        {
            try
            {
                await Task.Run(new Action(() =>
                {
                    driver.Navigate().GoToUrl(url);
                    for (int i = 0; i < times; i++)
                    {
                        Start();
                        SaveOrderData();
                    }
                    driver.Quit();
                }));
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion

        #region Registry

        public void SaveOrderData()
        {
            try
            {
                register.NewOrder();
                foreach (string item in items)
                {
                    string[] itemAsVector = item.Split(new char[3] { '-', 'x', '#' });
                    if (!int.TryParse(itemAsVector[3], out int result))
                    {
                        itemAsVector[0] += " " + itemAsVector[1];
                        itemAsVector[1] = itemAsVector[2];
                        itemAsVector[2] = itemAsVector[3];
                        itemAsVector[3] = itemAsVector[4];
                    }
                    itemAsVector[0] = Regex.Replace(itemAsVector[0], "á", "a");
                    itemAsVector[0] = Regex.Replace(itemAsVector[0], "Á", "A");
                    itemAsVector[0] = Regex.Replace(itemAsVector[0], "é", "e");
                    itemAsVector[0] = Regex.Replace(itemAsVector[0], "í", "i");

                    if (itemAsVector[0].StartsWith("Combo"))
                    {
                        register.Combo(itemAsVector[3], itemAsVector[1].Trim());
                        register.WriteArchive("", itemAsVector[3], itemAsVector[0], itemAsVector[1].Trim(), itemAsVector[2].Trim('\r', '\n', 'R', '$'));
                    }
                    else
                    {
                        register.Product(itemAsVector[3], itemAsVector[1].Trim());
                        register.WriteArchive(itemAsVector[3], "", itemAsVector[0], itemAsVector[1].Trim(), itemAsVector[2].Trim('\r', '\n', 'R', '$'));
                    }
                }
            }
            catch (DbUpdateException ex) { }
            catch (Exception ex) { throw ex; }
        }

        #endregion
    }
}
