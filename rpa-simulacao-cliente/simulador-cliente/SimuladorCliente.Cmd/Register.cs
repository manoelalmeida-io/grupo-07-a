﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Reflection;
using SimuladorCliente.Data.Context;
using SimuladorCliente.Data.Models;

namespace SimuladorCliente.Cmd
{
    public class Register
    {
        private int orderId;
        private int computerId;
        private string computerType;
        private string path = Path.GetTempPath() + "high-stonks\\";
        private string archivePath;

        public Register()
        {
            using (StreamReader sr = new StreamReader(path + "logs-monitoramento\\config.txt"))
            {
                string line = sr.ReadToEnd().Trim('\n');
                computerId = int.Parse(line.Split('|')[2]);
                computerType = line.Split('|')[3];
            }
        }

        public void NewOrder()
        {
            using (Context context = new Context())
            {
                using (DbContextTransaction transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        context.Database.ExecuteSqlCommand($"SELECT TOP 0 NULL FROM {typeof(Pedido).GetCustomAttribute<TableAttribute>().Name} WITH (TABLOCKX)");
                        context.Pedido.Add(new Pedido() { DataHora = DateTime.Now, IdLoja = 1, IdMaquina = computerId });
                        context.SaveChanges();
                        transaction.Commit();
                        orderId = context.Pedido.ToList().LastOrDefault().IdPedido;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
        }

        public void Product(string productId, string quantity)
        {
            using (Context context = new Context())
            {
                using (DbContextTransaction transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        context.Database.ExecuteSqlCommand($"SELECT TOP 0 NULL FROM {typeof(PedidoProduto).GetCustomAttribute<TableAttribute>().Name} WITH (TABLOCKX)");
                        context.PedidoProduto.Add(new PedidoProduto()
                        {
                            IdPedido = orderId,
                            IdProduto = int.Parse(productId),
                            Quantidade = int.Parse(quantity)
                        });
                        context.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
        }

        public void Combo(string comboId, string quantity)
        {
            using (Context context = new Context())
            {
                using (DbContextTransaction transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        context.Database.ExecuteSqlCommand($"SELECT TOP 0 NULL FROM {typeof(PedidoCombo).GetCustomAttribute<TableAttribute>().Name} WITH (TABLOCKX)");
                        context.PedidoCombo.Add(new PedidoCombo()
                        {
                            IdPedido = orderId,
                            IdCombo = int.Parse(comboId),
                            Quantidade = int.Parse(quantity)
                        });
                        context.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
        }

        private void CheckRegistryArchive()
        {
            archivePath = path + "client-simulator";
            DirectoryInfo directory = new DirectoryInfo(archivePath);
            if (!directory.Exists) directory.Create();
            archivePath += "\\registry.csv";
            if (!File.Exists(archivePath))
            {
                File.Create(archivePath).Close();
                using (StreamWriter sw = new StreamWriter(archivePath))
                    sw.WriteLine("OrderId;ProductId;ComboId;ProductName;Quantity;UnityPrice(R$);TotalPrice(R$);Date;ComputerId;ComputerType");
            }
        }

        public void WriteArchive(string productId, string comboId, string productName, string quantity, string totalPrice)
        {
            CheckRegistryArchive();
            double unityPrice = double.Parse(totalPrice.Replace('.', ',')) / int.Parse(quantity);
            using (StreamWriter sw = new StreamWriter(archivePath, true))
                sw.WriteLine($"{orderId};{productId};{comboId};{productName};{quantity};{unityPrice:N2};{totalPrice.Replace('.', ',')};{DateTime.Now.ToShortDateString()};{computerId};{computerType}");
        }
    }
}
