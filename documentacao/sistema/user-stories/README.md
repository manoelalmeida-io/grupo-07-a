# User Stories
------------------------------------------------

### Story 1

**Como** um gerente de TI **preciso** monitorar os totens **para** averiguar possíveis inconsistências no sistema que fariam os totens pararem de funcionar inesperadamente.

### Story 2

**Como** gerente de TI de uma rede de fast food **gostaria** de receber alertas sobre os caixas e totens **para** tomar medidas preventivas. Preferencialmente através do Slack

### Story 3

**Como** um gerente de operações **gostaria** de visualizar os dados de vendas das lojas **para** tomar decisões a respeito da criação de novos lanches e construção de novas unidades.

### Story 4

**Como** gerente global de uma rede de fast food **gostaria** de visualizar os dados de vendas de toda a rede em através de gráficos estatísticos **para** melhor leitura das informações. 

### Story 5

**Como** um gerente de operações, **gostaria** de receber notificações caso um totem esteja apresentando defeito, **pois** preciso que os todos os totens funcionem caso contrário ocorrerá um acúmulo de pessoas na fila.

### Story 6

**Como** um gerente de TI **preciso** visualizar em tempo real o uso de CPU, RAM e Disco **para** saber se os totens e caixas estão em operação.

### Story 7

**Como** um gerente de TI **preciso** visualizar os logs do sistema **para** saber se ouve uma alguma falha durante a execução do software dos totens e caixas.

### Story 8

**Como** um gerente de TI **preciso** que o sistema cadastre as máquinas no sistema e atualize automaticamente **para** que eu não precise cadastrar manualmente cada máquina e **para** que nenhuma máquina fique cadastrada com componentes desatualizados.

### Story 9

**Como** um gerente de TI **preciso** visualizar gráficos estatísticos de uso de CPU, RAM e disco **para** monitorar mais facilmente se uma das máquinas está apresentando falha na execução dos softwares instalados.

### Story 10

**Como** um gerente de operações de uma rede de fast food **gostaria** de visualizar de forma rápida se uma unidade está com muitas estações ociosas ou defeituosas **para** que o problema seja resolvido o mais rápido o possível.

### Story 11

**Como** um gerente de operações de uma rede de fast food **preciso** de métricas e estatísticas de vendas de lanches **para** tomar decisões corporativas a respeito dos produtos vendidos pela marca.

### Story 12

**Como** um gerente de operações de uma rede de fast food **preciso** de alertas de caso uma unidade esteja com seu funcionamento comprometido **para** agir o mais rápido o possível.