SELECT tb_modelo.nome, count(id_maquina) unidades FROM tb_maquina
	JOIN tb_loja ON tb_maquina.fk_loja = id_loja
	JOIN tb_modelo ON fk_modelo = id_modelo
	JOIN tb_grupo_loja ON tb_maquina.fk_loja = tb_grupo_loja.fk_loja
	JOIN tb_grupo_usuario ON tb_grupo_loja.fk_grupo = tb_grupo_usuario.fk_grupo
	WHERE fk_usuario = 46
	GROUP BY tb_modelo.nome;

SELECT semana, count(*) quantidade FROM (
	SELECT DISTINCT 
		DATEPART(Weekday, momento) semana, 
		DATEPART(Day, momento) dia, 
		DATEPART(Hour, momento) hora, 
		DATEPART(Minute, momento) minuto, id_maquina
	FROM tb_registro
		JOIN tb_maquina ON tb_registro.fk_maquina = id_maquina
		JOIN tb_loja ON tb_maquina.fk_loja = id_loja
		JOIN tb_modelo ON fk_modelo = id_modelo
		JOIN tb_grupo_loja ON tb_maquina.fk_loja = tb_grupo_loja.fk_loja
		JOIN tb_grupo_usuario ON tb_grupo_loja.fk_grupo = tb_grupo_usuario.fk_grupo
	WHERE momento > DATEADD(Week, -1, getdate()) AND alarme = 1 AND fk_usuario = 46
) as alertas
GROUP BY semana;

SELECT DATEPART(MONTH, momento) mes, tb_modelo.nome as modelo, 
	AVG(reg_cpu) as media_cpu,
	AVG(reg_memoria) as media_memoria,
	AVG(reg_disco) as media_disco
FROM tb_registro
	JOIN tb_maquina ON tb_registro.fk_maquina = id_maquina
	JOIN tb_loja ON tb_maquina.fk_loja = id_loja
	JOIN tb_modelo ON fk_modelo = id_modelo
	JOIN tb_grupo_loja ON tb_maquina.fk_loja = tb_grupo_loja.fk_loja
	JOIN tb_grupo_usuario ON tb_grupo_loja.fk_grupo = tb_grupo_usuario.fk_grupo
WHERE fk_usuario = 46 AND momento > DATEADD(Month, -6, getdate())
GROUP BY DATEPART(MONTH, momento), tb_modelo.nome;

SELECT tb_modelo.nome, count(DISTINCT id_maquina) unidades, quantidade as alertas FROM tb_maquina
	JOIN tb_loja ON tb_maquina.fk_loja = id_loja
	JOIN tb_modelo ON fk_modelo = id_modelo
	JOIN tb_grupo_loja ON tb_maquina.fk_loja = tb_grupo_loja.fk_loja
	JOIN tb_grupo_usuario ON tb_grupo_loja.fk_grupo = tb_grupo_usuario.fk_grupo
	JOIN (
		SELECT id_maquina maquina, count(*) quantidade FROM (
			SELECT DISTINCT 
				DATEPART(Weekday, momento) semana, 
				DATEPART(Day, momento) dia, 
				DATEPART(Hour, momento) hora, 
				DATEPART(Minute, momento) minuto, id_maquina
			FROM tb_registro
				JOIN tb_maquina ON tb_registro.fk_maquina = id_maquina
				JOIN tb_loja ON tb_maquina.fk_loja = id_loja
				JOIN tb_modelo ON fk_modelo = id_modelo
				JOIN tb_grupo_loja ON tb_maquina.fk_loja = tb_grupo_loja.fk_loja
				JOIN tb_grupo_usuario ON tb_grupo_loja.fk_grupo = tb_grupo_usuario.fk_grupo
			WHERE momento > DATEADD(Week, -1, getdate()) AND alarme = 1 AND fk_usuario = 46
		) as alertas
		GROUP BY id_maquina
	) as alertas ON id_maquina = maquina
	WHERE fk_usuario = 46
GROUP BY tb_modelo.nome, quantidade;

SELECT id_maquina, tipo, tb_loja.email, quantidade, 
	AVG(reg_cpu) as media_cpu,
	AVG(reg_memoria) as media_memoria,
	AVG(reg_disco) as media_disco
FROM tb_maquina
	JOIN tb_loja ON tb_maquina.fk_loja = id_loja
	JOIN tb_modelo ON fk_modelo = id_modelo
	JOIN tb_grupo_loja ON tb_maquina.fk_loja = tb_grupo_loja.fk_loja
	JOIN tb_grupo_usuario ON tb_grupo_loja.fk_grupo = tb_grupo_usuario.fk_grupo
	JOIN tb_registro ON fk_maquina = id_maquina
	JOIN (
		SELECT id_maquina maquina, count(*) quantidade FROM (
			SELECT DISTINCT 
				DATEPART(Weekday, momento) semana, 
				DATEPART(Day, momento) dia, 
				DATEPART(Hour, momento) hora, 
				DATEPART(Minute, momento) minuto, id_maquina
			FROM tb_registro
				JOIN tb_maquina ON tb_registro.fk_maquina = id_maquina
				JOIN tb_loja ON tb_maquina.fk_loja = id_loja
				JOIN tb_modelo ON fk_modelo = id_modelo
				JOIN tb_grupo_loja ON tb_maquina.fk_loja = tb_grupo_loja.fk_loja
				JOIN tb_grupo_usuario ON tb_grupo_loja.fk_grupo = tb_grupo_usuario.fk_grupo
			WHERE momento > DATEADD(Week, -1, getdate()) AND alarme = 1 AND fk_usuario = 46
		) as alertas
		GROUP BY id_maquina
	) as alertas ON id_maquina = maquina
WHERE momento > DATEADD(Week, -1, getdate()) AND fk_usuario = 46
GROUP BY id_maquina, tipo, tb_loja.email, quantidade;

SELECT DISTINCT id_modelo, tb_modelo.nome, quantidade FROM tb_registro
	JOIN tb_maquina ON tb_registro.fk_maquina = id_maquina
	JOIN tb_loja ON tb_maquina.fk_loja = id_loja
	JOIN tb_modelo ON fk_modelo = id_modelo
	JOIN tb_grupo_loja ON tb_maquina.fk_loja = tb_grupo_loja.fk_loja
	JOIN tb_grupo_usuario ON tb_grupo_loja.fk_grupo = tb_grupo_usuario.fk_grupo
	JOIN (
		SELECT id_maquina maquina, count(*) quantidade FROM (
			SELECT DISTINCT 
				DATEPART(Weekday, momento) semana, 
				DATEPART(Day, momento) dia, 
				DATEPART(Hour, momento) hora, 
				DATEPART(Minute, momento) minuto, id_maquina
			FROM tb_registro
				JOIN tb_maquina ON tb_registro.fk_maquina = id_maquina
				JOIN tb_loja ON tb_maquina.fk_loja = id_loja
				JOIN tb_modelo ON fk_modelo = id_modelo
				JOIN tb_grupo_loja ON tb_maquina.fk_loja = tb_grupo_loja.fk_loja
				JOIN tb_grupo_usuario ON tb_grupo_loja.fk_grupo = tb_grupo_usuario.fk_grupo
			WHERE alarme = 1 AND fk_usuario = 46
		) as alertas
		GROUP BY id_maquina
	) as alertas ON id_maquina = maquina
WHERE fk_usuario = 46;