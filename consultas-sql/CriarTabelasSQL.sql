/* CRIANDO ENTIDADES DO BANCO DE DADOS */

-- TABELA EMPRESA
CREATE TABLE tb_empresa (
	id_empresa INT PRIMARY KEY IDENTITY(1,1),
	nome VARCHAR(50) NOT NULL,
	senha VARCHAR(50) NOT NULL,
	email VARCHAR(50) NOT NULL,
	cnpj VARCHAR(20)
);
GO

-- TABELA LOJA
CREATE TABLE tb_loja (
	id_loja INT PRIMARY KEY IDENTITY(1,1),
	nome VARCHAR(50) NOT NULL,
	email VARCHAR(50) NOT NULL,
	senha VARCHAR(50) NOT NULL
);
GO

-- TABELA GRUPO
CREATE TABLE tb_grupo (
	id_grupo INT PRIMARY KEY IDENTITY(1,1),
	nome VARCHAR(50) NOT NULL
);
GO

-- TABELA USU�RIO
CREATE TABLE tb_usuario (
	id_usuario INT PRIMARY KEY IDENTITY(1,1),
	nome VARCHAR(50) NOT NULL,
	email VARCHAR(50) NOT NULL,
	senha VARCHAR(50) NOT NULL,
	max_cpu TINYINT CHECK (max_cpu <= 100),
	max_memoria TINYINT CHECK (max_memoria <= 100),
	max_disco TINYINT CHECK (max_disco <= 100)
);
GO

-- TABELA M�QUINA
CREATE TABLE tb_maquina (
	id_maquina INT PRIMARY KEY IDENTITY(1,1),
	tipo BIT -- CAIXA OU TOTEM
);
GO

-- TABELA MODELO
CREATE TABLE tb_modelo (
	id_modelo INT PRIMARY KEY IDENTITY(1,1),
	nome VARCHAR(50),
	cpu VARCHAR(100),
	memoria VARCHAR(100),
	disco VARCHAR(100)
);
GO

-- TABELA REGISTRO
CREATE TABLE tb_registro (
	momento DATETIME NOT NULL,
	reg_cpu TINYINT CHECK (reg_cpu <= 100),
	reg_memoria TINYINT CHECK (reg_memoria <= 100),
	reg_disco TINYINT CHECK (reg_disco <= 100),
	alarme BIT
);
GO

-- TABELA PRODUTO
CREATE TABLE tb_produto (
	id_produto INT PRIMARY KEY IDENTITY(1,1),
	nome VARCHAR(100),
	descricao VARCHAR(255),
	categoria VARCHAR(50),
	preco DECIMAL(10,2),
	image_url VARCHAR(150)
);
GO

-- TABELA COMBO
CREATE TABLE tb_combo (
	id_combo INT PRIMARY KEY IDENTITY(1,1),
	nome VARCHAR(100),
	descricao VARCHAR(255),
	categoria VARCHAR(50),
	preco DECIMAL(10,2),
	image_url VARCHAR(150)
);
GO

-- TABELA PEDIDO
CREATE TABLE tb_pedido (
	id_pedido INT PRIMARY KEY IDENTITY(1,1),
	datahora DATETIME NOT NULL,
);
GO

/* CRIANDO RELACIONAMENTOS DO BANCO DE DADOS */

-- RELACIONAMENTO EMPRESA POSSUI LOJA
ALTER TABLE tb_loja ADD fk_empresa INT NOT NULL;
ALTER TABLE tb_loja ADD FOREIGN KEY (fk_empresa) REFERENCES tb_empresa(id_empresa);
GO

-- RELACIONAMENTO LOJA TEM MAQUINA
ALTER TABLE tb_maquina ADD fk_loja INT NOT NULL;
ALTER TABLE tb_maquina ADD FOREIGN KEY (fk_loja) REFERENCES tb_loja(id_loja);
GO

-- RELACIONAMENTO MAQUINA PERTENCE A MODELO
ALTER TABLE tb_maquina ADD fk_modelo INT NOT NULL;
ALTER TABLE tb_maquina ADD FOREIGN KEY (fk_modelo) REFERENCES tb_modelo(id_modelo);
GO

-- RELACIONAMENTO MAQUINA REGISTRA REGISTRO (ENTIDADE FRACA)
ALTER TABLE tb_registro ADD fk_maquina INT NOT NULL;
ALTER TABLE tb_registro ADD FOREIGN KEY (fk_maquina) REFERENCES tb_maquina(id_maquina);
ALTER TABLE tb_registro ADD PRIMARY KEY (fk_maquina, momento);
GO

-- RELACIONAMENTO EMPRESA VENDE PRODUTO
ALTER TABLE tb_produto ADD fk_empresa INT NOT NULL;
ALTER TABLE tb_produto ADD FOREIGN KEY (fk_empresa) REFERENCES tb_empresa(id_empresa);
GO

-- RELACIONAMENTO EMPRESA VENDE COMBO
ALTER TABLE tb_combo ADD fk_empresa INT NOT NULL;
ALTER TABLE tb_combo ADD FOREIGN KEY (fk_empresa) REFERENCES tb_empresa(id_empresa);
GO

-- RELACIONAMENTO LOJA VENDE PEDIDO
ALTER TABLE tb_pedido ADD fk_loja INT NOT NULL;
ALTER TABLE tb_pedido ADD FOREIGN KEY (fk_loja) REFERENCES tb_loja(id_loja);
GO

-- RELACIONAMENTO USUARIO E EMPRESA
ALTER TABLE tb_usuario ADD fk_empresa INT NOT NULL;
ALTER TABLE tb_usuario ADD FOREIGN KEY (fk_empresa) REFERENCES tb_empresa(id_empresa);
GO

-- RELACIONAMENTO GRUPO E EMPRESA
ALTER TABLE tb_grupo ADD fk_empresa INT NOT NULL;
ALTER TABLE tb_grupo ADD FOREIGN KEY (fk_empresa) REFERENCES tb_empresa(id_empresa);

-- RELACIONAMENTO COMBO POSSUI PRODUTO
CREATE TABLE tb_produto_combo (
	fk_produto INT,
	fk_combo INT,
	quantidade TINYINT,
	FOREIGN KEY (fk_produto) REFERENCES tb_produto(id_produto),
	FOREIGN KEY (fk_combo) REFERENCES tb_combo(id_combo),
	PRIMARY KEY (fk_produto, fk_combo)
);
GO

-- RELACIONAMENTO PEDIDO CONT�M PRODUTO
CREATE TABLE tb_pedido_produto (
	fk_pedido INT,
	fk_produto INT,
	quantidade TINYINT,
	FOREIGN KEY (fk_pedido) REFERENCES tb_pedido(id_pedido),
	FOREIGN KEY (fk_produto) REFERENCES tb_produto(id_produto),
	PRIMARY KEY (fk_pedido, fk_produto)
);
GO

-- RELACIONAMENTO PEDIDO CONT�M COMBO
CREATE TABLE tb_pedido_combo (
	fk_pedido INT,
	fk_combo INT,
	quantidade TINYINT,
	FOREIGN KEY (fk_pedido) REFERENCES tb_pedido(id_pedido),
	FOREIGN KEY (fk_combo) REFERENCES tb_combo(id_combo),
	PRIMARY KEY (fk_pedido, fk_combo)
);
GO

-- RELACIONAMENTO PERTENCE A GRUPO
CREATE TABLE tb_grupo_loja (
	fk_grupo INT,
	fk_loja INT,
	FOREIGN KEY (fk_grupo) REFERENCES tb_grupo(id_grupo),
	FOREIGN KEY (fk_loja) REFERENCES tb_loja(id_loja),
	PRIMARY KEY (fk_grupo, fk_loja)
);
GO

-- RELACIONAMENTO GRUPO � ADMINISTRADO POR USUARIO
CREATE TABLE tb_grupo_usuario (
	fk_grupo INT,
	fk_usuario INT,
	FOREIGN KEY (fk_grupo) REFERENCES tb_grupo(id_grupo),
	FOREIGN KEY (fk_usuario) REFERENCES tb_usuario(id_usuario),
	PRIMARY KEY (fk_grupo, fk_usuario)
);
GO