const express = require('express');
const path = require('path');

const routes = require('./routes');

const app = express();

app.use(express.json());
app.use(routes);
app.use('/', express.static(path.resolve(__dirname, 'public')));

app.listen(2400);