var users =[];
async function cadastrar(event) {
  event.preventDefault();
  var form = document.forms['form_loja'];

  var data = {
    nome: form.elements['nome'].value,
    email: form.elements['email'].value,
    senha: form.elements['senha'].value,
    grupos: form.elements['grupos'].value,
    fk_empresa: sessionStorage.getItem('id')
  }

  const loading = document.getElementsByClassName('loader')[0];

  info.innerHTML = '';
  erro.innerHTML = '';

  try {
    loading.classList.remove('hidden');
    await axios.post('/loja', data);
    info.innerHTML = 'Loja cadastrada com sucesso';
    atualizar();
  }
  catch(err) {
    erro.innerHTML = 'Ocorreu um erro ao cadastrar loja';
  }
  finally {
    loading.classList.add('hidden');
  }


  return false;
}

async function atualizar() {
  const id = sessionStorage.getItem('id');
  const storeList = document.getElementById('store-list');
  users = await axios.get('/loja?empresa=' + id);

  storeList.innerHTML = '';

  users.data.forEach(store => {
    storeList.innerHTML += renderStore(store);
  });
}

function renderStore(store) {
  return `
    <div class="store">
      <span class="name">${store.nome}</span>
      <div class="content">
        <div>
          <span class="label">Email</span>
          <span class="value">${store.email}</span>
        </div>
        <div>
          <span class="label">Grupos</span>
          <span class="value">${store.grupos == null ? 'Nenhum' : store.grupos}</span>
        </div>
      </div>
    </div>
  `;
}

function filtrar(event){
  const storeList = document.getElementById('store-list');
  var filtro = users.data.filter((item)=>item.nome.toLowerCase().includes(event.target.value.toLowerCase()));
  storeList.innerHTML = '';

  filtro.forEach(store => {
    storeList.innerHTML += renderStore(store);
  });
}

window.onload = atualizar();