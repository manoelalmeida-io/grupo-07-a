var groups =[];
async function cadastrar(event) {
  event.preventDefault();
  var form = document.forms['form_grupo'];

  var data = {
    nome: form.elements['nome'].value,
    fk_empresa: sessionStorage.getItem('id')
  }

  const loading = document.getElementsByClassName('loader')[0];

  info.innerHTML = '';
  erro.innerHTML = '';

  try {
    loading.classList.remove('hidden');
    await axios.post('/grupo', data);
    info.innerHTML = 'Grupo cadastrado com sucesso';
    atualizar();
  }
  catch (err) {
    erro.innerHTML = 'Ocorreu um erro ao cadastrar grupo';
  }
  finally {
    loading.classList.add('hidden');
  }

  return false;
}

async function atualizar() {
  const id = sessionStorage.getItem('id');
  const groupList = document.getElementById('group-list');
  groups = await axios.get('/grupo?empresa=' + id);

  groupList.innerHTML = '';

  groups.data.forEach(groups => {
    groupList.innerHTML += renderGroup(groups);
  });
}

function renderGroup(group) {
  return `
    <div class="group">
      <span class="name">${group.nome}</span>
    </div>
  `;
}

function filtrar(event){
  const groupList = document.getElementById('group-list');
  var filtro = groups.data.filter((item)=>item.nome.toLowerCase().includes(event.target.value.toLowerCase()));
  groupList.innerHTML = '';
  filtro.forEach(groups => {
    groupList.innerHTML += renderGroup(groups);
  });
}

window.onload = atualizar();