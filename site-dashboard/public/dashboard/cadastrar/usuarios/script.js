 var users = []
async function cadastrar(event) {
  event.preventDefault();
  var form = document.forms['form_usuario'];

  var data = {
    nome: form.elements['nome'].value,
    email: form.elements['email'].value,
    senha: form.elements['senha'].value,
    grupos: form.elements['grupos'].value,
    fk_empresa: sessionStorage.getItem('id')
  }

  const loading = document.getElementsByClassName('loader')[0];

  info.innerHTML = '';
  erro.innerHTML = '';

  try {
    loading.classList.remove('hidden');
    await axios.post('/usuarios', data);
    info.innerHTML = 'Usuário cadastrado com sucesso';
    atualizar();
  } 
  catch (err) {
    erro.innerHTML = 'Ocorreu um erro ao cadastrar usuário';
  }
  finally {
    loading.classList.add('hidden');
  }

  return false;
}

async function atualizar() {
  const id = sessionStorage.getItem('id');
  const userList = document.getElementById('user-list');
  users = await axios.get('/usuarios?empresa=' + id);

  userList.innerHTML = '';

  users.data.forEach(user => {
    userList.innerHTML += renderUser(user);
  });
}

function renderUser(user) {
  return `
    <div class="user">
      <span class="name">${user.nome}</span>
      <div class="content">
        <div>
          <span class="label">Email</span>
          <span class="value">${user.email}</span>
        </div>
        <div>
          <span class="label">Persissões</span>
          <span class="value">${user.grupos == null ? 'Nenhuma' : user.grupos}</span>
        </div>
      </div>
    </div>
  `;
}
function filtrar(event){
  const userList = document.getElementById('user-list');
  var filtro = users.data.filter((item)=>item.nome.toLowerCase().includes(event.target.value.toLowerCase()));
  userList.innerHTML = '';

  filtro.forEach(user => {
    userList.innerHTML += renderUser(user);
  });
}

window.onload = atualizar();