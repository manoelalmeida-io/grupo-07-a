function redirecionarLogin() {
  window.location = '/login';
}

function verificarAutenticacao() {
  let emailUsuario = sessionStorage.getItem('email');

  if (emailUsuario) {
    validarSessao();  
  }
  else {
    redirecionarLogin();
  }
}

async function validarSessao() {
  try {
    await axios.post('/auth/usuario', {
      email: sessionStorage.getItem('email'),
    });
  }
  catch(err) {
    redirecionarLogin();
  }
}

async function encerrarSessao(event) {
  event.preventDefault();

  const email = sessionStorage.getItem('email');

  if (email == undefined) {
    redirecionarLogin();
  }

  try {
    await axios.post('/logout', { email });

    sessionStorage.removeItem('id');
    sessionStorage.removeItem('nome');
    sessionStorage.removeItem('email');
    sessionStorage.removeItem('tipo');

    redirecionarLogin();
  } 
  catch (err) {
    console.error(err);
  }
}

verificarAutenticacao();