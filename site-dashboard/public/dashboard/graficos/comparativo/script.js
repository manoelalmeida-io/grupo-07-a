Chart.defaults.global.defaultFontColor = '#ffffff';
Chart.defaults.global.defaultFontFamily = 'Roboto, sans-serif'
Chart.defaults.global.legend.labels.usePointStyle = true;

const mediaModelo = new MediaModelo();
mediaModelo.obterDadosGrafico();

function trocarDados(obj, valor) {
  mediaModelo.trocarDados(valor);

  const modelOptionList = document.getElementsByClassName('model-option');
  
  for (let i = 0; i < modelOptionList.length; i++) {
    modelOptionList[i].classList.remove('selected');
  }

  obj.classList.add('selected');
}

async function obterModelos() {
  const id = sessionStorage.getItem('id');
  const gridModelos = document.getElementById('grid-modelos');

  const resposta = await axios.get(`/dashboard/comparativo/modelos/${id}`);

  gridModelos.innerHTML = '';

  resposta.data.forEach((modelo) => {
    gridModelos.innerHTML += renderModelo(modelo);
  });
}

function renderModelo(modelo) {
  return `
    <div class="modelo">
      <div class="content">
        <div class="title-bar">
          <h2>${modelo.nome.length < 20 ? modelo.nome : modelo.nome.substr(0, 17) + '...'}</h2>
        </div>
        <p>Unidades: ${modelo.unidades}</p>
        <p>Alertas essa semana: ${modelo.alertas}</p>
      </div>
      <img class="background" src="../../../assets/icons/devices-white-18dp.svg" alt="">
    </div>
  `;
}

async function obterMaquinas() {
  const id = sessionStorage.getItem('id');
  const tableMaquinas = document.getElementById('maquinas');

  const resposta = await axios.get(`/dashboard/comparativo/maquinas/${id}`);

  tableMaquinas.innerHTML = '';

  resposta.data.forEach((maquina) => {
    tableMaquinas.innerHTML += renderMaquina(maquina);
  });
}

function renderMaquina(maquina) {
  return `
    <tr>
      <td>${maquina.id_maquina}</td>
      <td>${maquina.tipo}</td>
      <td>${maquina.email}</td>
      <td>${maquina.quantidade}</td>
      <td>${maquina.media_cpu}%</td>
      <td>${maquina.media_memoria}%</td>
      <td>${maquina.media_disco}%</td>
    </tr>
  `;
}

/* var alertaModeloCtx = document.getElementById('alerta-modelo').getContext('2d'); */
/* var alertaModeloConfig = {
  type: 'doughnut',
  data: {
    labels: ['Optplex182', 'HP-234', 'LENOVO-382'],
    datasets: [{
      label: 'Modelo 1',
      data: [4, 9, 8],
      backgroundColor: ['#3949AB', '#00ACC1', '#00897B'],
      borderColor: ['#3949AB', '#00ACC1', '#00897B'],
      borderWidth: 4,
    }],
  },
  options: {
    responsive: true,
    maintainAspectRatio: false,
    tooltips: {
      backgroundColor: '#FFF',
      titleFontColor: '#000',
      bodyFontColor: '#000',
      displayColors: false,
    }
  },
}; */

/* var alertaModeloChart = new Chart(alertaModeloCtx, alertaModeloConfig); */
obterModelos();
obterMaquinas();