class MediaModelo {

  constructor() {
    this.colors = ['#3949AB', '#00ACC1', '#00897B', '#00E676', '#E040FB']
    this.month = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 
      'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']
    this.options = {
      responsive: true,
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          },
          scaleLabel: {
            display: true,
            labelString: 'Porcentagem'
          }
        }]
      },
      title: {
        display: true
      },
      tooltips: {
        intersect: false,
        backgroundColor: '#FFF',
        titleFontColor: '#000',
        bodyFontColor: '#000',
        displayColors: false
      }
    }
    this.data = {};
    this.response = {};
  }

  async obterDadosGrafico() {
    const id = sessionStorage.getItem('id');
    this.response = await axios.get(`/dashboard/comparativo/media/modelo/${id}`);

    this.trocarDados('media_cpu');
  }

  async trocarDados(tipo) {
    const tipoNome = tipo == 'media_cpu' ? 'CPU' : tipo == 'media_memoria' ? 'memória' : 'disco';
    const labels = this.response.data.map((item) => this.month[item.mes]);

    this.options.title.text = `Uso de ${tipoNome} por modelo`;
    this.options.tooltips.callbacks = {
      label: (tooltipItem, data) => {
        var label = data.datasets[tooltipItem.datasetIndex].label;
        return label + ': ' + tooltipItem.yLabel + `% de uso de ${tipoNome}`;
      }
    }

    const datasets = [];
    
    this.response.data[0].dados.forEach((item, index) => {
      const data = this.response.data.map((dado) => dado.dados[index][tipo]);
      datasets.push({
        label: item.modelo,
        data: data,
        borderColor: this.colors[index],
        borderWidth: 4,
        fill: false
      })
    });

    this.data = {
      labels: labels,
      datasets: datasets
    };

    this.plotarGrafico();
  }

  async plotarGrafico() {
    const modelosCtx = document.getElementById('modelos').getContext('2d');
    if (window._mediaModelo) {
      window._mediaModelo.destroy();
    }
    
    window._mediaModelo = new Chart(modelosCtx, {
      type: 'line',
      data: this.data,
      options: this.options
    });
  }
}

