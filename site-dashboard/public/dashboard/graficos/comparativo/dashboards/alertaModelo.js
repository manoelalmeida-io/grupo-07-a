class AlertaModelo {

  constructor() {
    this.options =  {
      responsive: true,
      maintainAspectRatio: false,
      tooltips: {
        backgroundColor: '#FFF',
        titleFontColor: '#000',
        bodyFontColor: '#000',
        displayColors: false,
      }
    };
    this.data = {};
  }

  async obterDadosGrafico() {
    const id = sessionStorage.getItem('id');

    const response = await axios.get(`/dashboard/comparativo/qtd/alertas/modelo/${id}`);

    const labels = response.data.map((item) => item.nome.length < 20 ? item.nome : item.nome.substr(0, 17) + '...');
    const values = response.data.map((item) => item.quantidade);

    this.data = {
      labels: labels,
      datasets: [{
        data: values,
        backgroundColor: ['#3949AB', '#00ACC1', '#00897B', '#00E676', '#E040FB'],
        borderColor: ['#3949AB', '#00ACC1', '#00897B', '#00E676', '#E040FB'],
        borderWidth: 4,
      }],
    };

    this.plotarGrafico();
  }

  async plotarGrafico() {
    const alertaModeloCtx = document.getElementById('alerta-modelo').getContext('2d');
    new Chart(alertaModeloCtx, {
      type: 'doughnut',
      data: this.data,
      options: this.options
    });
  }
}

new AlertaModelo().obterDadosGrafico();