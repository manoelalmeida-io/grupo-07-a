class NumeroIncidentes {

  constructor() {
    this.weekdays = ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'];
    this.options = {
      responsive: true,
      maintainAspectRatio: false,
      tooltips: {
        backgroundColor: '#FFF',
        titleFontColor: '#000',
        bodyFontColor: '#000',
        displayColors: false,
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
            precision: 0
          }
        }]
      },
      legend: {
        display: false
      }
    };
    this.data = {};
  }

  async obterDadosGrafico() {
    const id = sessionStorage.getItem('id');

    const response = await axios.get(`/dashboard/comparativo/qtd/alertas/semana/${id}`);

    const labels = response.data.map((item) => this.weekdays[item.dia_semana]);
    const values = response.data.map((item) => item.alertas);

    this.data = {
      labels: labels,
      datasets: [{
        label: 'Incidentes',
        data: values,
        borderColor: '#3949AB',
        borderWidth: 4,
        fill: false,
        lineTension: 0.3
      }],
    };

    this.plotarGrafico();
  }

  async plotarGrafico() {
    const numeroIncidentesCtx = document.getElementById('numero-incidentes').getContext('2d');
    new Chart(numeroIncidentesCtx, {
      type: 'line',
      data: this.data,
      options: this.options
    });
  }
}

new NumeroIncidentes().obterDadosGrafico();