const checkbox = document.getElementById('navbar-button');
const navbar = document.getElementById('navbar');

checkbox.addEventListener('change', function () {
	if (this.checked) {
		navbar.classList.add('open');
	} else {
		navbar.classList.remove('open');
	}
});

const monitoramentoCtx = document.getElementById('monitoramento').getContext('2d');
new Chart(monitoramentoCtx, {
	type: 'line',
	data: {
		labels: ['1:00:00', '1:00:01', '1:00:02', '1:00:03', '1:00:00', '1:00:01', '1:00:02', '1:00:03'],
		datasets: [{
			label: 'CPU',
			data: [ 20, 20, 23, 24, 20, 20, 23, 24 ],
			borderColor: '#3949AB',
			borderWidth: 4,
			fill: false,
			lineTension: 0.3
		},
		{
			label: 'Memória',
			data: [ 20 * 3, 20 * 3, 23 * 3, 24 * 3, 20 * 3, 20 * 3, 23 * 3, 24 * 3 ],
			borderColor: '#00ACC1',
			borderWidth: 4,
			fill: false,
			lineTension: 0.3
		},
		{
			label: 'Disco',
			data: [ 55, 55, 55, 55, 55, 55, 55, 55 ],
			borderColor: '#00897B',
			borderWidth: 4,
			fill: false,
			lineTension: 0.3
		}],
	},
	options: {
		responsive: true,
		maintainAspectRatio: false,
		tooltips: {
			backgroundColor: '#FFF',
			titleFontColor: '#000',
			bodyFontColor: '#000',
			displayColors: false,
		},
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero: true,
					precision: 0
				}
			}]
		},
		legend: {
			display: true
		},
		title: {
			display: true,
			text: 'Monitoramento - Maquina 1',
			fontColor: '#FFF',
			fontSize: 16,
			fontFamily: 'Roboto'
		}
	}
});

const operacaoCtx = document.getElementById('maquinas-operacao').getContext('2d');
new Chart(operacaoCtx, {
	type: 'doughnut',
	data: {
		labels: ['Lidadas', 'Desligadas'],
		datasets: [{
			data: [ 9, 2 ],
			backgroundColor: ['#3949AB', '#5C5C5C'],
			borderColor: ['#292929', '#292929'],
			borderWidth: 4,
			fill: true,
		}],
	},
	options: {
		responsive: true,
		maintainAspectRatio: false,
		tooltips: {
			backgroundColor: '#FFF',
			titleFontColor: '#000',
			bodyFontColor: '#000',
			displayColors: false,
		},
		legend: {
			display: true
		},
		title: {
			display: true,
			text: 'Disponibilidade',
			fontColor: '#FFF',
			fontSize: 16,
			fontFamily: 'Roboto'
		}
	}
});

const relacaoCtx = document.getElementById('relacao-alerta').getContext('2d');
new Chart(relacaoCtx, {
	type: 'bar',
	data: {
		labels: ['janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho'],
		datasets: [
			{
				type: 'line',
				label: 'Alertas',
				data: [ 3 * 10, 2 * 10, 2 * 10, 3 * 10, 6 * 10, 7 * 10 ],
				borderColor: '#FF5252',
				borderWidth: 4,
				fill: false,
			},
			{
				type: 'bar',
				label: 'CPU',
				data: [ 9, 5, 5, 7, 7, 8, 9, 10 ],
				backgroundColor: '#3949AB',
				borderWidth: 4,
				fill: true,
			},
			{
				type: 'bar',
				label: 'Memória',
				data: [ 80, 82, 89, 82, 92, 90 ],
				backgroundColor: '#00ACC1',
				borderWidth: 4,
				fill: true,
			},
			{
				type: 'bar',
				label: 'Disco',
				data: [ 40, 42, 44, 44, 46, 50 ],
				backgroundColor: '#00897B',
				borderWidth: 4,
				fill: true,
			}
		],
	},
	options: {
		responsive: true,
		maintainAspectRatio: false,
		tooltips: {
			backgroundColor: '#FFF',
			titleFontColor: '#000',
			bodyFontColor: '#000',
			displayColors: false,
		},
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero: true,
					precision: 0
				}
			}]
		},
		legend: {
			display: true
		},
		title: {
			display: true,
			text: 'Métricas x Alertas',
			fontColor: '#FFF',
			fontSize: 16,
			fontFamily: 'Roboto'
		}
	}
});