function redirecionarLogin() {
  window.location = '/login';
}

function verificarAutenticacao() {
  let emailUsuario = sessionStorage.getItem('email');

  if (emailUsuario) {
    validarSessao();  
  }
  else {
    redirecionarLogin();
  }
}

async function validarSessao() {
  try {
    await axios.post('/auth/loja', {
      email: sessionStorage.getItem('email'),
    });
  }
  catch(err) {
    redirecionarLogin();
  }
}

verificarAutenticacao();