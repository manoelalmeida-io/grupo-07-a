class Relacao {

  constructor() {
    this.options = {
      maintainAspectRatio: false,
      responsive: true,
      legend: {
        display: true
      },
      animation: {
        animateScale: true,
        animateRotate: true
      },
      plugins: {
        labels: {
          fontSize: 32,
        }
      }
    };
    this.data = {};
  }

  async obterDadosGrafico() {
    const id = sessionStorage.getItem('id');

    const response = await axios.get(`/dashboard/comercial/relacao/vendas/${id}`);

    const labels = response.data.map((item) => item.tipo);
    const values = response.data.map((item) => item.quantidade);

    this.data = {
      datasets: [{
        data: values,
        backgroundColor: [
          '#3949AB',
          '#00ACC1'
        ],
        borderColor: [
          '#3949AB',
          '#00ACC1'
        ]
      }],
      labels: labels
    };

    this.plotarGrafico();
  }

  async plotarGrafico() {
    const ctx_donut = document.getElementById('donut_chart').getContext('2d');
    new Chart(ctx_donut, {
      type: 'doughnut',
      data: this.data,
      options: this.options
    });
  }
}

new Relacao().obterDadosGrafico();