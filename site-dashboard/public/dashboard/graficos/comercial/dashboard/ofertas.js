class Ofertas {

  constructor() {
    this.options = {
      tooltips: {
        mode: 'index',
        intersect: false
      },
      responsive: true,
      scales: {
        xAxes: [{
          stacked: true,
        }],
        yAxes: [{
          stacked: true
        }]
      },
      plugins: {
        labels: {
          render: function(args) {
            return args.percentage > 0 ? `${args.percentage}%` : '';
          }
        }
      }
    };
    this.data = {};
  }

  async obterDados() {
    const id = sessionStorage.getItem('id');

    const response = await axios.get(`/dashboard/comercial/ofertas/${id}`);

    const labels = response.data.map((item) => item.nome);
    const normal = response.data.map((item) => item.normal);
    const combo = response.data.map((item) => item.combo);

    this.data = {
      labels: labels,
      datasets: [{
        label: 'Venda por unidade',
        backgroundColor: '#3949AB',
        data: normal
      },
      {
        label: 'Venda por combo',
        backgroundColor: '#00ACC1',
        data: combo
      }],
    };

    this.plotarGrafico();
  }

  async plotarGrafico() {
    const ctx_bar = document.getElementById('bar_chart').getContext('2d');
    new Chart(ctx_bar, {
      type: 'bar',
      data: this.data,
      options: this.options
    })
  }
}

new Ofertas().obterDados();