class Semana {
  
  constructor() {
    this.colors = ['#3949AB', '#00ACC1', '#00897B', '#00E676', '#E040FB', '#AB47BC', '#FF8F00']
    this.weekdays = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado']
    this.options = {
      responsive: true,
      maintainAspectRatio: true,
      tooltips: {
        backgroundColor: '#FFF',
        titleFontColor: '#000',
        bodyFontColor: '#000',
        displayColors: false,
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
            precision: 0
          }
        }]
      },
      legend: {
        display: true
      },
      title: {
        display: true,
        text: 'Total de vendas durante a semana (individual e combo)',
        fontColor: '#FFF',
        fontSize: 16,
        fontFamily: 'Roboto'
      },
      plugins: {
        labels: false
      }
    };
    this.data = {};
  }

  async obterDadosGrafico(categoria) {
    const id = sessionStorage.getItem('id');

    const response = await axios.get(`/dashboard/comercial/vendas/semana/${id}/${categoria}`);
    const labels = response.data.map((item) => this.weekdays[item.dia_semana]);

    const datasets = [];

    response.data[0].dados.forEach((item, index) => {
      const data = response.data.map((dado) => dado.dados[index].quantidade);
      datasets.push({
        label: item.produto,
        data: data,
        borderColor: this.colors[index],
        backgroundColor: this.colors[index],
        fill: true
      });
    });

    console.log(datasets);

    this.data = {
      labels: labels,
      datasets: datasets,
    }

    this.plotarGrafico();
  }

  async plotarGrafico() {
    const lineChartCtx = document.getElementById('line-chart').getContext('2d');
    
    if (window.semanaGrafico) {
      window.semanaGrafico.destroy();
    }
    
    window.semanaGrafico = new Chart(lineChartCtx, {
      type: 'bar',
      data: this.data,
      options: this.options
    });
  }
}