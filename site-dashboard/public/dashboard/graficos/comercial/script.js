Chart.defaults.global.defaultFontColor = '#ffffff';
Chart.defaults.global.defaultFontFamily = 'Roboto, sans-serif';
Chart.defaults.global.legend.labels.usePointStyle = true;

new Semana().obterDadosGrafico('lanches');

function trocarGrafico() {
  new Semana().obterDadosGrafico(selectCategoria.value);
}