var inForm = document.getElementById('signin');
var upForm = document.getElementById('signup');

function signup() {
  inForm.classList.add('hidden');
  upForm.classList.remove('hidden');
}

function signin() {
  upForm.classList.add('hidden');
  inForm.classList.remove('hidden');
}

async function cadastrar(event) {
  event.preventDefault();

  var form = document.forms['form_cadastro'];

  if (form.elements['senha'].value !== form.elements['confirmarSenha'].value) {
    alert('senhas não coincidem');
    return false;
  }

  var data = {
    nome: form.elements['nome'].value,
    email: form.elements['email'].value,
    cnpj: form.elements['cnpj'].value,
    senha: form.elements['senha'].value,
  }

  const loaders = document.getElementsByClassName('loader');
  
  try {
    loaders[1].classList.remove('hidden');
    await axios.post('/empresa', data);
  }
  catch (err) {
    console.log(err);
  }
  finally {
    loaders[1].classList.add('hidden');
  }

  var login = document.forms['form_login'];
  login.elements['email'].value = form.elements['email'].value;

  signin();

  return false;
}

async function logar(event) {
  event.preventDefault();

  var form = document.forms['form_login'];

  var data = {
    email: form.elements['email'].value,
    senha: form.elements['senha'].value
  }

  const loaders = document.getElementsByClassName('loader');
  let resposta = {};

  try {
    loaders[0].classList.remove('hidden');
    resposta = await axios.post('/login', data);
  }
  catch (err) {
    console.log(err);
  }
  finally {
    loaders[0].classList.add('hidden');
  }

  if(!resposta.data || !resposta.data.id_empresa) {
    erroLogin.innerHTML = 'Email ou senha incorretos';
    return false;
  }
  
  sessionStorage.setItem('id', resposta.data.id_empresa);
  sessionStorage.setItem('nome', resposta.data.nome);
  sessionStorage.setItem('email', resposta.data.email);
  sessionStorage.setItem('tipo', resposta.data.tipo);

  if (resposta.data.tipo == 'empresa') {
    window.location = "../dashboard/cadastrar/usuarios";
  }

  if (resposta.data.tipo == 'loja') {
    window.location = "../dashboard/graficos/monitoramento";
  }

  if (resposta.data.tipo == 'usuario') {
    window.location = "../dashboard/graficos/comparativo";
  }

  return false;
}