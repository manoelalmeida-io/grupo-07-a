const { Router } = require('express');

const Usuario = require('./controllers/usuario');
const Loja = require('./controllers/loja');
const Grupo = require('./controllers/grupo');
const Empresa = require('./controllers/empresa');
const Dashboard = require('./controllers/dashboard');
const Auth = require('./controllers/auth');
const DashboardComparativo = require('./controllers/dashboardComparativo');
const DashboardComercial = require('./controllers/dashboardComercial');

const router = Router();

const usuario = new Usuario();
const loja = new Loja();
const grupo = new Grupo();
const empresa = new Empresa();
const dashboard = new Dashboard();
const auth = new Auth();
const dashComparativo = new DashboardComparativo();
const dashComercial = new DashboardComercial();

router.get('/usuarios', usuario.mostrarTodos);
router.get('/usuarios/:id', usuario.mostrar);
router.post('/usuarios', usuario.gravar);
router.put('/usuarios/:id', usuario.atualizar);
router.delete('/usuarios/:id', usuario.deletar);

router.get('/loja', loja.mostrarTodos);
router.get('/loja/:id', loja.mostrar);
router.post('/loja', loja.gravar);
router.put('/loja/:id', loja.atualizar);
router.delete('/loja/:id', loja.deletar);

router.get('/grupo', grupo.mostrarTodos);
router.get('/grupo/:id', grupo.mostrar);
router.post('/grupo', grupo.gravar);
router.put('/grupo/:id', grupo.atualizar);
router.delete('/grupo/:id', grupo.deletar);

router.get('/empresa', empresa.mostrarTodos);
router.get('/empresa/:id', empresa.mostrar);
router.post('/empresa', empresa.gravar);
router.put('/empresa/:id', empresa.atualizar);
router.delete('/empresa/:id', empresa.deletar);

router.get('/dashboard', dashboard.mostrarTodos);
router.get('/dashboard/maquina/:id', dashboard.mostrar);

router.post('/auth/empresa', auth.autenticarEmpresa);
router.post('/auth/usuario', auth.autenticarUsuario);
router.post('/auth/loja', auth.autenticarLoja);
router.post('/login', auth.logar);
router.post('/logout', auth.encerrarSessao);

router.get('/hardware/:id', dashboard.mostrar);

router.get('/dashboard/comparativo/qtd/maquina/modelo/:id_usuario', dashComparativo.modelo);
router.get('/dashboard/comparativo/qtd/alertas/semana/:id_usuario', dashComparativo.alertas);
router.get('/dashboard/comparativo/qtd/alertas/modelo/:id_usuario', dashComparativo.alertaModelo);
router.get('/dashboard/comparativo/media/modelo/:id_usuario', dashComparativo.mediaModelo);
router.get('/dashboard/comparativo/modelos/:id_usuario', dashComparativo.todosModelos);
router.get('/dashboard/comparativo/maquinas/:id_usuario', dashComparativo.todasMaquinas);

router.get('/dashboard/comercial/relacao/vendas/:id_empresa', dashComercial.relacaoVenda);
router.get('/dashboard/comercial/ofertas/:id_empresa', dashComercial.diferentesOfertas);
router.get('/dashboard/comercial/vendas/semana/:id_empresa/:categoria', dashComercial.vendasSemana);

module.exports = router;