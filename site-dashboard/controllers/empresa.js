const connection = require('../connection');

class Empresa{
    async gravar(request, response){
        var dados = request.body;
        var resposta = await connection("tb_empresa").returning("*").insert(dados);
        return response.json(resposta);
    }

    async deletar(request, response){
        var id = request.params.id;
        await connection("tb_empresa").where("id_empresa", id).delete();
        response.json({ resposta: "Empresa deletada" });
    }

    async atualizar(request, response){
        var id = request.params.id;
        var dados = request.body;
        
        var resposta = await connection("tb_Empresa").returning("*").where("id_empresa", id).update(dados);
        response.json(resposta);
    }

    async mostrar(request, response){
        var id = request.params.id;
        var mostrar = await connection("tb_empresa").where("id_empresa", id).select("*");
        response.json(mostrar);
    }

    async mostrarTodos(request, response){
        var mostrar = await connection("tb_empresa").select("*");
        response.json(mostrar);
    }
}
module.exports = Empresa