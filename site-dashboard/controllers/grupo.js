const connection = require('../connection');

class Grupo{
    async gravar(request, response){
        var dados = request.body;
        var resposta = await connection("tb_grupo").returning("*").insert(dados);
        return response.json(resposta);
    }

    async deletar(request, response){
        var id = request.params.id;
        await connection("tb_grupo").where("id_grupo", id).delete();
        response.json({ resposta: "Grupo deletado" });
    }

    async atualizar(request, response){
        var id = request.params.id;
        var dados = request.body;
        
        var resposta = await connection("tb_grupo").returning("*").where("id_grupo", id).update(dados);
        response.json(resposta);
    }

    async mostrar(request, response){
        var id = request.params.id;
        var mostrar = await connection("tb_grupo").where("id_grupo", id).select("*");
        response.json(mostrar);
    }

    async mostrarTodos(request, response){
        var empresa = request.query.empresa;
        var mostrar = await connection("tb_grupo").where('fk_empresa', empresa).select("*");
        response.json(mostrar);
    }
}

module.exports = Grupo