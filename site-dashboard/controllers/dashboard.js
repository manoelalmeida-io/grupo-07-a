const connection = require('../connection');

class Dashboard{ 
    async mostrar(request, response){
        var id = request.params.id;
        var mostrar = await connection("tb_registro").where("fk_maquina", id).select("*");
        response.json(mostrar);
    }

    async mostrarTodos(request, response){
        var mostrar = await connection("tb_empresa").select("*");
        response.json(mostrar);
    }
}
module.exports = Dashboard