const connection = require('../connection');

class Loja{
    async gravar(request, response){
        var dados = request.body;
        var grupos = dados.grupos.split(",").map(value => `'${value.trim()}'`).join(',');

        var dadosLoja = {
            nome: dados.nome,
            email: dados.email,
            senha: dados.senha,
            fk_empresa: dados.fk_empresa
        }

        try {
            var trx = await connection.transaction();
            
            var loja = await trx("tb_loja").returning("*").insert(dadosLoja);
            await trx.from("tb_grupo_loja").insert(
                connection.raw(`SELECT grupos.*, id_loja FROM tb_loja
                    JOIN (SELECT id_grupo FROM tb_grupo WHERE nome in (${grupos})) 
                        as grupos on id_loja = ${loja[0].id_loja}`)
            );

            trx.commit();

        } catch(err) {
            console.error(err);
            trx.rollback();
        }

        const resposta = {
            ...loja[0],
            grupos
        }
        
        return response.json(resposta);
    }

    async deletar(request, response){
        var id = request.params.id;
        await connection("tb_loja").where("id_loja", id).delete();
        response.json({ resposta: "Loja deletada" });
    }

    async atualizar(request, response){
        var id = request.params.id;
        var dados = request.body;
        
        var resposta = await connection("tb_loja").returning("*").where("id_loja", id).update(dados);
        response.json(resposta);
    }

    async mostrar(request, response){
        var id = request.params.id;
        var mostrar = await connection("tb_loja").where("id_loja", id).select("*");
        response.json(mostrar);
    }

    async mostrarTodos(request, response){
        var empresa = request.query.empresa;
        var mostrar = await connection.raw(`
            SELECT id_loja, tb_loja.nome, email, senha, STRING_AGG(tb_grupo.nome, ', ') as grupos FROM tb_loja
                LEFT JOIN tb_grupo_loja ON id_loja = fk_loja
                LEFT JOIN tb_grupo ON id_grupo = fk_grupo
                WHERE tb_loja.fk_empresa = ${empresa}
                GROUP BY id_loja, tb_loja.nome, email, senha;
        `);
        response.json(mostrar);
    }
}
module.exports = Loja