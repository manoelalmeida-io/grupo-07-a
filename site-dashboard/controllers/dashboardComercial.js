const connection = require('../connection');

class DashboardComercial {

  async relacaoVenda(request, response) {
    const { id_empresa } = request.params;
    
    const res = await connection.raw(`
      SELECT tipo, COUNT(*) quantidade FROM tb_pedido
        JOIN tb_maquina ON id_maquina = fk_maquina
        JOIN tb_loja ON tb_pedido.fk_loja = id_loja
        JOIN tb_empresa ON fk_empresa = id_empresa
      WHERE id_empresa = ${id_empresa}
      GROUP BY tipo
    `);

    const serialized = res.map((item) => ({
      tipo: item.tipo ? 'totem' : 'caixa',
      quantidade: item.quantidade
    }));

    response.json(serialized);
  }

  async diferentesOfertas(request, response) {
    const { id_empresa } = request.params;

    const res = await connection.raw(`
      SELECT nome, normal, combo FROM tb_produto
        LEFT JOIN (
          SELECT id_produto produto, SUM(tb_pedido_produto.quantidade) normal FROM tb_pedido_produto
            JOIN tb_pedido ON id_pedido = fk_pedido
            JOIN tb_produto ON id_produto = fk_produto
            JOIN tb_maquina ON id_maquina = fk_maquina
            JOIN tb_loja ON tb_pedido.fk_loja = id_loja
            JOIN tb_empresa ON tb_loja.fk_empresa = id_empresa
          WHERE id_empresa = ${id_empresa}
          GROUP BY id_produto
        ) AS venda_normal ON venda_normal.produto = id_produto
        LEFT JOIN (
          SELECT id_produto produto, SUM(tb_pedido_combo.quantidade) combo FROM tb_pedido_combo
            JOIN tb_pedido ON id_pedido = fk_pedido
            JOIN tb_produto_combo ON tb_produto_combo.fk_combo = tb_pedido_combo.fk_combo
            JOIN tb_produto ON id_produto = fk_produto
            JOIN tb_maquina ON id_maquina = fk_maquina
            JOIN tb_loja ON tb_pedido.fk_loja = id_loja
            JOIN tb_empresa ON tb_loja.fk_empresa = id_empresa
          WHERE id_empresa = ${id_empresa}
          GROUP BY id_produto
        ) AS venda_combo ON venda_combo.produto = id_produto
      WHERE fk_empresa = ${id_empresa} AND categoria = 'lanches';
    `);

    const serialized = res.map((item) => ({
      nome: item.nome,
      normal: item.normal ? item.normal : 0,
      combo: item.combo ? item.combo : 0,
    }));

    response.json(serialized);
  }

  async vendasSemana(request, response) {
    const { id_empresa, categoria } = request.params;

    const res = [];

    const data = await connection.raw(`
      SELECT nome, SUM(normal) quantidade, dia_semana FROM tb_produto
        JOIN (
          SELECT id_produto produto, SUM(tb_pedido_produto.quantidade) normal, DATEPART(WEEKDAY, datahora) dia_semana FROM tb_pedido_produto
            JOIN tb_pedido ON id_pedido = fk_pedido
            JOIN tb_produto ON id_produto = fk_produto
            JOIN tb_maquina ON id_maquina = fk_maquina
            JOIN tb_loja ON tb_pedido.fk_loja = id_loja
            JOIN tb_empresa ON tb_loja.fk_empresa = id_empresa
          WHERE id_empresa = 1 AND datahora > DATEADD(Week, -1, getdate())
          GROUP BY id_produto, DATEPART(WEEKDAY, datahora)
          UNION SELECT id_produto produto, SUM(tb_pedido_combo.quantidade) combo, DATEPART(WEEKDAY, datahora) dia_semana FROM tb_pedido_combo
            JOIN tb_pedido ON id_pedido = fk_pedido
            JOIN tb_produto_combo ON tb_produto_combo.fk_combo = tb_pedido_combo.fk_combo
            JOIN tb_produto ON id_produto = fk_produto
            JOIN tb_maquina ON id_maquina = fk_maquina
            JOIN tb_loja ON tb_pedido.fk_loja = id_loja
            JOIN tb_empresa ON tb_loja.fk_empresa = id_empresa
          WHERE id_empresa = 1 AND datahora > DATEADD(Week, -1, getdate())
          GROUP BY id_produto, DATEPART(WEEKDAY, datahora)
        ) AS venda_normal ON venda_normal.produto = id_produto
      WHERE fk_empresa = ${id_empresa} AND categoria = '${categoria}'
      GROUP BY nome, dia_semana;
    `);

    const produtos = data
      .map((item) => item.nome)
      .filter((value, index, self) => self.indexOf(value) == index);

    for (let i = 6; i >= 0; i--) {
      const date = new Date();
      date.setDate(date.getDay() - i);

      res.push({ dia_semana: date.getDay(), dados: [] });

      const filteredData = data.filter((item) => item.dia_semana == date.getDay() + 1);
      const insertIndex = res.findIndex((item) => item.dia_semana == date.getDay());

      produtos.forEach((produto) => {
        const modeloData = filteredData.filter((item) => item.nome == produto);
        res[insertIndex].dados.push({ 
          produto, 
          quantidade: modeloData[0] ? modeloData[0].quantidade : 0, 
        });
      });
    }

    response.json(res);
  }
}

module.exports = DashboardComercial;