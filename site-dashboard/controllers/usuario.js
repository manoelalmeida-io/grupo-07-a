const connection = require('../connection');

class Usuario{

    async gravar(request, response){
        var dados = request.body;
        var grupos = dados.grupos.split(",").map(value => `'${value.trim()}'`).join(',');

        var dadosUsuario = {
            nome: dados.nome,
            email: dados.email,
            senha: dados.senha,
            fk_empresa: dados.fk_empresa
        }

        try {
            var trx = await connection.transaction();

            var usuario = await trx("tb_usuario").returning("*").insert(dadosUsuario);
            await trx.from("tb_grupo_usuario").insert(
                connection.raw(`SELECT grupos.*, id_usuario FROM tb_usuario
                    JOIN (SELECT id_grupo FROM tb_grupo WHERE nome in (${grupos})) 
                        as grupos on id_usuario = ${usuario[0].id_usuario}`)
            );

            // const grupo_usuario = await trx("tb_grupo_usuario").where("fk_usuario", usuario[0].id_usuario).select("*");
            // console.log(grupo_usuario);

            trx.commit();
        } catch(err) {
            console.error(err);
            trx.rollback();
        }

        var resposta = {
            ...usuario[0],
            grupos
        }

        return response.json(resposta);
    }

    async deletar(request, response){
        var id = request.params.id;
        await connection("tb_usuario").where("id_usuario", id).delete();
        response.json({ resposta: "Usuário deletado" });
    }

    async atualizar(request, response){
        var id = request.params.id;
        var dados = request.body;
        
        var resposta = await connection("tb_usuario").returning("*").where("id_usuario", id).update(dados);
        response.json(resposta);
    }

    async mostrar(request, response){
        var id = request.params.id;
        var mostrar = await connection("tb_usuario").where("id_usuario", id).select("*");
        response.json(mostrar);
    }

    async mostrarTodos(request, response){
        var empresa = request.query.empresa;
        var mostrar = await connection.raw(`
            SELECT id_usuario, tb_usuario.nome, email, senha, STRING_AGG(tb_grupo.nome, ', ') as grupos FROM tb_usuario
                LEFT JOIN tb_grupo_usuario ON id_usuario = fk_usuario
                LEFT JOIN tb_grupo ON id_grupo = fk_grupo
                WHERE tb_usuario.fk_empresa = ${empresa}
                GROUP BY id_usuario, tb_usuario.nome, email, senha;
        `);

        response.json(mostrar);
    }
}

module.exports = Usuario