const connection = require('../connection');

let sessoes = [];

class Auth {

  async logar(request, response) {
    const {email, senha} = request.body;

    const usuarios = await connection('tb_empresa')
      .union(
        connection.select('id_loja', 'nome', 'email', connection.raw(`'loja' as tipo`)).from('tb_loja')
          .where({ email, senha })
      )
      .union(
        connection.select('id_usuario', 'nome', 'email', connection.raw(`'usuario' as tipo`)).from('tb_usuario')
          .where({ email, senha })
      )
      .where({ email, senha }).select('id_empresa', 'nome', 'email', connection.raw(`'empresa' as tipo`));


    if (usuarios.length === 0) {
      response.status(403).json({ mensagem: "Login e/ou senha inválidos" });
    } 
    else {
      sessoes.push(usuarios[0]);
      response.json(usuarios[0]);
    }
  }

  async autenticarEmpresa(request, response) {
    const { email } = request.body;

    const filtered = sessoes.filter((user) => user.email === email && user.tipo === 'empresa');

    if (filtered.length === 0) {
      response.sendStatus(403);
    }
    else {
      response.json({ mensagem: "Usuário tem sessão ativa" });
    }
  }

  async autenticarLoja(request, response) {
    const { email } = request.body;

    const filtered = sessoes.filter((user) => user.email === email && user.tipo === 'loja');

    if (filtered.length === 0) {
      response.sendStatus(403);
    }
    else {
      response.json({ mensagem: "Usuário tem sessão ativa" });
    }
  }

  async autenticarUsuario(request, response) {
    const { email } = request.body;

    const filtered = sessoes.filter((user) => user.email === email && user.tipo === 'usuario');

    if (filtered.length === 0) {
      response.sendStatus(403);
    }
    else {
      response.json({ mensagem: "Usuário tem sessão ativa" });
    }
  }

  async encerrarSessao(request, response) {
    const { email } = request.body;

    const filtered = sessoes.filter((user) => user.email != email);

    sessoes = filtered;

    response.sendStatus(200);
  }
}

module.exports = Auth;