const connection = require('../connection');

class DashboardComparativo {

  async modelo(request, response) {
    const { id_usuario } = request.params;

    const resposta = await connection.raw(`
      SELECT tb_modelo.nome, count(id_maquina) unidades FROM tb_maquina
        JOIN tb_loja ON tb_maquina.fk_loja = id_loja
        JOIN tb_modelo ON fk_modelo = id_modelo
        JOIN tb_grupo_loja ON tb_maquina.fk_loja = tb_grupo_loja.fk_loja
        JOIN tb_grupo_usuario ON tb_grupo_loja.fk_grupo = tb_grupo_usuario.fk_grupo
        WHERE fk_usuario = ${id_usuario}
        GROUP BY tb_modelo.nome;
    `);

    response.json(resposta);
  }

  async alertas(request, response) {
    const { id_usuario } = request.params;

    const res = [];
  
    const data = await connection.raw(`
      SELECT semana, count(*) quantidade FROM (
        SELECT DISTINCT 
          DATEPART(Weekday, momento) semana, 
          DATEPART(Day, momento) dia, 
          DATEPART(Hour, momento) hora, 
          DATEPART(Minute, momento) minuto, id_maquina
        FROM tb_registro
          JOIN tb_maquina ON tb_registro.fk_maquina = id_maquina
          JOIN tb_loja ON tb_maquina.fk_loja = id_loja
          JOIN tb_modelo ON fk_modelo = id_modelo
          JOIN tb_grupo_loja ON tb_maquina.fk_loja = tb_grupo_loja.fk_loja
          JOIN tb_grupo_usuario ON tb_grupo_loja.fk_grupo = tb_grupo_usuario.fk_grupo
        WHERE momento > DATEADD(Week, -1, getdate()) AND alarme = 1 AND fk_usuario = ${id_usuario}
      ) as alertas
      GROUP BY semana;
    `);

    for (let i = 6; i >= 0; i--) {
      const date = new Date();
      date.setDate(date.getDate() - i);

      const filteredData = data.filter((item) => item.semana == date.getDay() + 1);

      if (filteredData.length > 0) {
        res.push({ dia_semana: date.getDay(), alertas: filteredData[0].quantidade });
      }
      else {
        res.push({ dia_semana: date.getDay(), alertas: 0 });
      }
    }

    response.json(res);
  }

  async mediaModelo(request, response) {
    const { id_usuario } = request.params;

    const res = [];

    const data = await connection.raw(`
      SELECT DATEPART(MONTH, momento) mes, tb_modelo.nome as modelo, 
        AVG(reg_cpu) as media_cpu,
        AVG(reg_memoria) as media_memoria,
        AVG(reg_disco) as media_disco
      FROM tb_registro
        JOIN tb_maquina ON tb_registro.fk_maquina = id_maquina
        JOIN tb_loja ON tb_maquina.fk_loja = id_loja
        JOIN tb_modelo ON fk_modelo = id_modelo
        JOIN tb_grupo_loja ON tb_maquina.fk_loja = tb_grupo_loja.fk_loja
        JOIN tb_grupo_usuario ON tb_grupo_loja.fk_grupo = tb_grupo_usuario.fk_grupo
      WHERE fk_usuario = ${id_usuario} AND momento > DATEADD(Month, -6, getdate())
      GROUP BY DATEPART(MONTH, momento), tb_modelo.nome;
    `);

    const modelos = data
      .map((item) => item.modelo)
      .filter((value, index, self) => self.indexOf(value) == index);

    for (let i = 5; i >= 0; i--) {
      const date = new Date();
      date.setMonth(date.getMonth() - i);

      res.push({ mes: date.getMonth(), dados: [] });

      const filteredData = data.filter((item) => item.mes == date.getMonth() + 1);
      const insertIndex = res.findIndex((item) => item.mes == date.getMonth());

      modelos.forEach((modelo) => {
        const modeloData = filteredData.filter((item) => item.modelo == modelo);
        res[insertIndex].dados.push({ 
          modelo, 
          media_cpu: modeloData[0] ? modeloData[0].media_cpu : 0, 
          media_memoria: modeloData[0] ? modeloData[0].media_memoria : 0, 
          media_disco: modeloData[0] ? modeloData[0].media_disco : 0, 
        });
      });
    }

    response.json(res);
  }

  async alertaModelo(request, response) {
    const { id_usuario } = request.params;

    const res = await connection.raw(`
      SELECT DISTINCT id_modelo, tb_modelo.nome, quantidade FROM tb_registro
        JOIN tb_maquina ON tb_registro.fk_maquina = id_maquina
        JOIN tb_loja ON tb_maquina.fk_loja = id_loja
        JOIN tb_modelo ON fk_modelo = id_modelo
        JOIN tb_grupo_loja ON tb_maquina.fk_loja = tb_grupo_loja.fk_loja
        JOIN tb_grupo_usuario ON tb_grupo_loja.fk_grupo = tb_grupo_usuario.fk_grupo
        JOIN (
          SELECT id_maquina maquina, count(*) quantidade FROM (
            SELECT DISTINCT 
              DATEPART(Weekday, momento) semana, 
              DATEPART(Day, momento) dia, 
              DATEPART(Hour, momento) hora, 
              DATEPART(Minute, momento) minuto, id_maquina
            FROM tb_registro
              JOIN tb_maquina ON tb_registro.fk_maquina = id_maquina
              JOIN tb_loja ON tb_maquina.fk_loja = id_loja
              JOIN tb_modelo ON fk_modelo = id_modelo
              JOIN tb_grupo_loja ON tb_maquina.fk_loja = tb_grupo_loja.fk_loja
              JOIN tb_grupo_usuario ON tb_grupo_loja.fk_grupo = tb_grupo_usuario.fk_grupo
            WHERE alarme = 1 AND fk_usuario = ${id_usuario}
          ) as alertas
          GROUP BY id_maquina
        ) as alertas ON id_maquina = maquina
      WHERE fk_usuario = ${id_usuario};
    `)

    response.json(res);
  }

  async todosModelos(request, response) {
    const { id_usuario } = request.params;

    const res = await connection.raw(`
      SELECT tb_modelo.nome, count(DISTINCT id_maquina) unidades, quantidade as alertas FROM tb_maquina
        JOIN tb_loja ON tb_maquina.fk_loja = id_loja
        JOIN tb_modelo ON fk_modelo = id_modelo
        JOIN tb_grupo_loja ON tb_maquina.fk_loja = tb_grupo_loja.fk_loja
        JOIN tb_grupo_usuario ON tb_grupo_loja.fk_grupo = tb_grupo_usuario.fk_grupo
        JOIN (
          SELECT id_maquina maquina, count(*) quantidade FROM (
            SELECT DISTINCT 
              DATEPART(Weekday, momento) semana, 
              DATEPART(Day, momento) dia, 
              DATEPART(Hour, momento) hora, 
              DATEPART(Minute, momento) minuto, id_maquina
            FROM tb_registro
              JOIN tb_maquina ON tb_registro.fk_maquina = id_maquina
              JOIN tb_loja ON tb_maquina.fk_loja = id_loja
              JOIN tb_modelo ON fk_modelo = id_modelo
              JOIN tb_grupo_loja ON tb_maquina.fk_loja = tb_grupo_loja.fk_loja
              JOIN tb_grupo_usuario ON tb_grupo_loja.fk_grupo = tb_grupo_usuario.fk_grupo
            WHERE momento > DATEADD(Week, -1, getdate()) AND alarme = 1 AND fk_usuario = ${id_usuario}
          ) as alertas
          GROUP BY id_maquina
        ) as alertas ON id_maquina = maquina
        WHERE fk_usuario = ${id_usuario}
      GROUP BY tb_modelo.nome, quantidade;
    `);

    response.json(res);
  }

  async todasMaquinas(request, response) {
    const { id_usuario } = request.params;

    const res = await connection.raw(`
      SELECT id_maquina, tipo, tb_loja.email, quantidade, 
        AVG(reg_cpu) as media_cpu,
        AVG(reg_memoria) as media_memoria,
        AVG(reg_disco) as media_disco
      FROM tb_maquina
        JOIN tb_loja ON tb_maquina.fk_loja = id_loja
        JOIN tb_modelo ON fk_modelo = id_modelo
        JOIN tb_grupo_loja ON tb_maquina.fk_loja = tb_grupo_loja.fk_loja
        JOIN tb_grupo_usuario ON tb_grupo_loja.fk_grupo = tb_grupo_usuario.fk_grupo
        JOIN tb_registro ON fk_maquina = id_maquina
        JOIN (
          SELECT id_maquina maquina, count(*) quantidade FROM (
            SELECT DISTINCT 
              DATEPART(Weekday, momento) semana, 
              DATEPART(Day, momento) dia, 
              DATEPART(Hour, momento) hora, 
              DATEPART(Minute, momento) minuto, id_maquina
            FROM tb_registro
              JOIN tb_maquina ON tb_registro.fk_maquina = id_maquina
              JOIN tb_loja ON tb_maquina.fk_loja = id_loja
              JOIN tb_modelo ON fk_modelo = id_modelo
              JOIN tb_grupo_loja ON tb_maquina.fk_loja = tb_grupo_loja.fk_loja
              JOIN tb_grupo_usuario ON tb_grupo_loja.fk_grupo = tb_grupo_usuario.fk_grupo
            WHERE momento > DATEADD(Week, -1, getdate()) AND alarme = 1 AND fk_usuario = ${id_usuario}
          ) as alertas
          GROUP BY id_maquina
        ) as alertas ON id_maquina = maquina
      WHERE momento > DATEADD(Week, -1, getdate()) AND fk_usuario = ${id_usuario}
      GROUP BY id_maquina, tipo, tb_loja.email, quantidade;
    `);

    const serialized = res.map((
      { id_maquina, tipo, email, quantidade, media_cpu, media_memoria, media_disco }) => {
      
        return {
          id_maquina,
          tipo: tipo ? 'Totem' : 'Caixa',
          email,
          quantidade,
          media_cpu,
          media_memoria,
          media_disco
        }
    });

    response.json(serialized);
  }
}

module.exports = DashboardComparativo;